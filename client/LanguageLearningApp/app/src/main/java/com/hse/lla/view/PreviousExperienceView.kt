package com.hse.lla.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.lifecycle.ViewModelProvider
import com.hse.lla.R
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.INITIAL_TEST_TAKEN
import com.hse.lla.viewmodel.PreviousExperienceViewModel

class PreviousExperienceView : AppCompatActivity() {
    private var viewModel: PreviousExperienceViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_previous_experience)

        viewModel = ViewModelProvider(this)
            .get(PreviousExperienceViewModel::class.java)
            .apply { state.observe(this@PreviousExperienceView, ::updateState) }

        val yesBtn: Button = findViewById(R.id.btn_yes)
        yesBtn.setOnClickListener {
            viewModel!!.onYes()
        }

        val noBtn: Button = findViewById(R.id.btn_no)
        noBtn.setOnClickListener {
            viewModel!!.onNo()
        }
    }

    private fun updateState(state: PreviousExperienceViewModel.State) {
        when (state) {
            is PreviousExperienceViewModel.State.GoToTest -> launchTestPrompt()
            is PreviousExperienceViewModel.State.SkipTest -> launchAssignedLevelActivity()
        }
    }

    private fun launchTestPrompt() {
        val intent = Intent(this, TestPromptView::class.java)
        startActivity(intent)
    }

    private fun launchAssignedLevelActivity() {
        val intent = Intent(this, LanguageLevelView::class.java)
        intent.putExtra(INITIAL_TEST_TAKEN, false)
        startActivity(intent)
    }
}
package com.hse.lla.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.ViewModelProvider
import com.hse.lla.R
import com.hse.lla.di.AppComponentHolder
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants
import com.hse.lla.viewmodel.DownloadCurriculumViewModel

class DownloadCurriculumView : AppCompatActivity() {

    private lateinit var viewModel: DownloadCurriculumViewModel

    private lateinit var progressSpinner: ProgressBar
    private lateinit var btnRetry: Button
    private lateinit var textViewDownloadInProgress: TextView
    private lateinit var textViewDownloadFailed: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_download_curriculum)

        progressSpinner = findViewById(R.id.progress_bar_download)
        btnRetry = findViewById(R.id.btn_retry_download)
        textViewDownloadInProgress = findViewById(R.id.textview_wait_for_download)
        textViewDownloadFailed = findViewById(R.id.textview_download_failed)

        btnRetry.setOnClickListener {
            viewModel.startDownload()
        }

        viewModel = ViewModelProvider(this)
            .get(DownloadCurriculumViewModel::class.java)
            .apply { state.observe(this@DownloadCurriculumView, ::updateState) }
        AppComponentHolder.getComponent().inject(viewModel)
        viewModel.startDownload()
    }

    private fun updateState(state: DownloadCurriculumViewModel.State) {
        when (state) {
            is DownloadCurriculumViewModel.State.DownloadStarted -> displayDownloadStarted()
            is DownloadCurriculumViewModel.State.DownloadFinished -> goToLessons()
            is DownloadCurriculumViewModel.State.DownloadFailed -> displayDownloadFailed()
        }
    }

    private fun goToLessons() {
        val prefs = getSharedPreferences("com.hse.lla", MODE_PRIVATE)
        prefs!!.edit().putBoolean(IntentAndSharedPrefsConstants.FRW_COMPLETED, true).apply()
        finish()
        val intent = Intent(this, LessonsMapView::class.java)
        startActivity(intent)
    }

    private fun displayDownloadStarted() {
        progressSpinner.visibility = View.VISIBLE
        btnRetry.visibility = View.INVISIBLE
        textViewDownloadInProgress.visibility = View.VISIBLE
        textViewDownloadFailed.visibility = View.INVISIBLE
    }

    private fun displayDownloadFailed() {
        progressSpinner.visibility = View.INVISIBLE
        btnRetry.visibility = View.VISIBLE
        textViewDownloadInProgress.visibility = View.INVISIBLE
        textViewDownloadFailed.visibility = View.VISIBLE
    }
}
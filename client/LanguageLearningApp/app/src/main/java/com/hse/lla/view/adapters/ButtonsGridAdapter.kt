package com.hse.lla.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import com.hse.lla.R
import com.hse.lla.viewmodel.LessonViewModel


class ButtonsGridAdapter(private val context: Context,
                         private val viewModel: LessonViewModel, ): BaseAdapter() {

    override fun getCount(): Int = viewModel.currTask?.choices?.size ?: 0

    override fun getItem(i: Int) = null

    override fun getItemId(i: Int): Long = 0

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup?): View {

        return if (view == null) {
            val inflater: LayoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val btnView: View = inflater.inflate(R.layout.gridview_single_button, null)
            
            val gridBtn: Button = btnView.findViewById(R.id.btn_in_grid)
            gridBtn.text = viewModel.currTask?.choices?.get(i) ?: ""
            gridBtn.setOnClickListener {
                viewModel.userAnswer = gridBtn.text.toString()
                viewModel.onAnswerSubmitted()
            }
            btnView
        } else {
            view
        }
    }
}
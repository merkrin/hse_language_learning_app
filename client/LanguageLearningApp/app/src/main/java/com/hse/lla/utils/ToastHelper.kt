package com.hse.lla.utils

import android.content.Context
import android.widget.Toast

class ToastHelper {
    companion object {
        fun showToast(context: Context, msg: String) {
            Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
        }
    }
}
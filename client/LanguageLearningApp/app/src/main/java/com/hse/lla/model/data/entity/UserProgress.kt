package com.hse.lla.model.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_progress")
data class UserProgress(
    @PrimaryKey val userId: Int,
    @ColumnInfo val overallProgress: Int,
    @ColumnInfo val numMeatballs: Int,
    @ColumnInfo val level: Int
)
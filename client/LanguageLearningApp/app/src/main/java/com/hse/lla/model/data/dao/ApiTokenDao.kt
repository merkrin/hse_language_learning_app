package com.hse.lla.model.data.dao

import androidx.room.Dao
import androidx.room.Query

@Dao
interface ApiTokenDao {
    @Query("INSERT INTO api_token VALUES(0, :token)")
    suspend fun insert(token: String)

    @Query("SELECT token FROM api_token LIMIT 1")
    suspend fun get(): String
}
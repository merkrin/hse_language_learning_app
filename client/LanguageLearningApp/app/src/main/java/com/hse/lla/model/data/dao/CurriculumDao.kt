package com.hse.lla.model.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.hse.lla.model.data.entity.Lesson
import com.hse.lla.model.data.entity.LessonsUnit
import com.hse.lla.model.data.entity.Level

@Dao
interface CurriculumDao {
    @Query("SELECT progress FROM curriculum LIMIT 1")
    suspend fun getUserProgress(): Int

    @Query("SELECT * FROM units")
    suspend fun getUnits(): List<LessonsUnit>

    @Query("SELECT * FROM lessons WHERE difficulty = :level")
    suspend fun getLessonsForLevel(level: Int): List<Lesson>

    @Query("SELECT progress FROM levels WHERE id = :level LIMIT 1")
    suspend fun getLevelProgress(level: Int): Int

    @Query("UPDATE levels SET progress = :newProgress WHERE id = :level")
    suspend fun saveLevelProgress(level: Int, newProgress: Int)

    @Insert
    suspend fun insertLevel(level: Level)

    @Insert
    suspend fun insertLevels(levels: List<Level>)
}
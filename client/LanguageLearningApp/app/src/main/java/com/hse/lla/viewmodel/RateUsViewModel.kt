package com.hse.lla.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hse.lla.model.data.entity.Feedback
import com.hse.lla.model.repo.FeedbackRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

class RateUsViewModel: ViewModel() {
    private val mutableState = MutableLiveData<State>()
    val state: LiveData<State> = mutableState

    @Inject
    lateinit var feedbackRepository: FeedbackRepository

    fun onRatingSubmitted(rating: Int, comment: String?) {
        viewModelScope.launch {
            feedbackRepository.insertFeedback(Feedback(0, rating, comment))
        }.invokeOnCompletion {
            mutableState.postValue(State.RatingSubmitted)
        }
    }

    sealed class State {
        object RatingSubmitted : State()
    }
}
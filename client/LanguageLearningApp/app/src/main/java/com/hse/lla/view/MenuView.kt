package com.hse.lla.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.hse.lla.R

class MenuView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_menu)

        val btnUserProfile: Button = findViewById(R.id.btn_user_profile)
        btnUserProfile.setOnClickListener {
            redirect(UserProfileView::class.java)
        }

        val btnShop: Button = findViewById(R.id.btn_shop)
        btnShop.setOnClickListener {
            // TODO
        }

        val btnRateUs: Button = findViewById(R.id.btn_rate_us)
        btnRateUs.setOnClickListener {
            redirect(RateUsView::class.java)
        }
    }

    private fun redirect(cls: Class<out AppCompatActivity>) {
        finish()
        val intent = Intent(this, cls)
        startActivity(intent)
    }
}
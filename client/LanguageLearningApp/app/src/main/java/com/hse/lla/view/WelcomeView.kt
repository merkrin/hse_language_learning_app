package com.hse.lla.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import androidx.lifecycle.ViewModelProvider
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.SignInButton
import com.hse.lla.R
import com.hse.lla.di.AppComponentHolder
import com.hse.lla.utils.GoogleSignInHelper
import com.hse.lla.utils.ToastHelper
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.FRW_COMPLETED
import com.hse.lla.viewmodel.WelcomeViewModel


class WelcomeView : AppCompatActivity() {
    private lateinit var viewModel: WelcomeViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_welcome)

        viewModel = ViewModelProvider(this)
            .get(WelcomeViewModel::class.java)
            .apply { state.observe(this@WelcomeView, ::updateState) }
        viewModel.googleSignInHelper = GoogleSignInHelper(this)
        AppComponentHolder.getComponent().inject(viewModel)
        viewModel.init()

        val signInBtn: SignInButton = findViewById(R.id.btn_sign_in)
        signInBtn.setOnClickListener {
            viewModel.onSignInClick()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GoogleSignInHelper.RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            viewModel.handleSignIn(task)
        }
    }

    private fun updateState(state: WelcomeViewModel.State) {
        when (state) {
            is WelcomeViewModel.State.SignedInFirst -> goNext(true)
            is WelcomeViewModel.State.SignedInSilent -> goNext(false)
        }
    }

    private fun goNext(isFirstSignIn: Boolean) {
        val prefs = getSharedPreferences("com.hse.lla", MODE_PRIVATE)
        val frwCompleted = prefs.getBoolean(FRW_COMPLETED, false)
        if (isFirstSignIn) {
            ToastHelper.showToast(this, getString(R.string.str_welcome_sign_in_successful))
        }
        val intent: Intent = if (isFirstSignIn || !frwCompleted) {
            Intent(this, FillUserInfoView::class.java)
        } else {
            Intent(this, LessonsMapView::class.java)
        }
        startActivity(intent)
    }
}
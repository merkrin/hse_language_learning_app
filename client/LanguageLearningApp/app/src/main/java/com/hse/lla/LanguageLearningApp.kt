package com.hse.lla

import android.app.Application
import android.util.Log
import com.hse.lla.di.AppComponentHolder
import com.hse.lla.model.data.AppDatabase
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LanguageLearningApp: Application() {

    override fun onCreate() {
        super.onCreate()
        AppComponentHolder.init(applicationContext)
        val db = AppDatabase.getInstance(applicationContext)
        GlobalScope.launch {
            db.unitDao().getLessons(0)
        }
        Log.i("APP", "Application.onCreate() called")
    }
}
package com.hse.lla.network.data.response

data class LessonProgressResponse(
    val lessonId: Long,
    val mistakes: Int,
    val progress: Int,
    val retries: Int,
    val status: String
)

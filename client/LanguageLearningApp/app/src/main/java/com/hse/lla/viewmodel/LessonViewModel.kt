package com.hse.lla.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hse.lla.model.data.entity.enums.TaskType
import com.hse.lla.model.data.entity.Task
import com.hse.lla.model.repo.LessonRepository
import com.hse.lla.model.repo.ProgressUpdateHelper
import kotlinx.coroutines.launch
import javax.inject.Inject

class LessonViewModel: ViewModel() {
    @Inject
    lateinit var repository: LessonRepository

    @Inject
    lateinit var progressUpdateHelper: ProgressUpdateHelper

    private var totalTasks = 0
    private var correctAnswers = 0
    private var earnedMeatballs = 0
    var lessonId = 0L

    private val mutableState = MutableLiveData<State>()
    val state: LiveData<State> = mutableState

    var userAnswer = ""
    var currTask: Task? = null
    var isInitialTest: Boolean = false

    fun onInited() {
        viewModelScope.launch { totalTasks = repository.getTasks(lessonId)?.size!! }
            .invokeOnCompletion { goToNextQuestion() }
    }

    fun onAnswerSubmitted() {
        val result = repository.checkAnswer(userAnswer, currTask!!.id)
        if (result) {
            ++correctAnswers
        }
        userAnswer = ""
        mutableState.postValue(State.GoToResult(result))
    }

    fun goToNextQuestion() {
        currTask = repository.getNextTask()
        if (currTask == null) {
            if (isInitialTest) {
                mutableState.postValue(State.GoToTestResult(totalTasks!!, correctAnswers))
            } else {
                viewModelScope.launch {
                    updateProgress()
                }.invokeOnCompletion {
                    mutableState.postValue(State.GoToLessonResult(totalTasks, correctAnswers, earnedMeatballs))
                }
            }
        } else {
            mutableState.postValue(State.GoToNextQuestion(TaskType.fromInt(currTask!!.type)))
        }
    }

    suspend fun updateProgress() {
        progressUpdateHelper.saveAll(lessonId, correctAnswers, totalTasks)
        earnedMeatballs = progressUpdateHelper.getEarnedMeatballs()
    }

    sealed class State {
        data class GoToResult(val correctAnswer: Boolean) : State()
        data class GoToNextQuestion(val taskType: TaskType): State()
        data class GoToLessonResult(val totalTasks: Int, val correctAnswers: Int, val earnedMeatballs: Int) : State()
        data class GoToTestResult(val totalTasks: Int, val correctAnswers: Int): State()
    }
}
package com.hse.lla.view

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.DatePicker
import android.widget.EditText
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.hse.lla.R
import com.hse.lla.databinding.ViewFillUserInfoBinding
import com.hse.lla.di.AppComponentHolder
import com.hse.lla.utils.ToastHelper
import com.hse.lla.viewmodel.FillUserInfoViewModel
import java.util.*

class FillUserInfoView : AppCompatActivity() {
    private lateinit var viewModel: FillUserInfoViewModel

    lateinit var binding: ViewFillUserInfoBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.view_fill_user_info)
        binding.lifecycleOwner = this

        viewModel = ViewModelProvider(this)
            .get(FillUserInfoViewModel::class.java)
            .apply { state.observe(this@FillUserInfoView, ::updateState) }
        AppComponentHolder.getComponent().inject(viewModel)
        viewModel.loadUser()
    }

    private fun loadUi() {
        binding.fillUserInfoViewModel = viewModel

        val saveBtn: Button = findViewById(R.id.btn_save)
        saveBtn.setOnClickListener {
            viewModel.onSave()
        }

        val birthdayEditText: EditText = findViewById(R.id.edittext_birthday)
        birthdayEditText.setOnClickListener {
            viewModel.onBirthdayEditTextClick()
        }
    }

    private fun updateState(state: FillUserInfoViewModel.State) {
        when (state) {
            is FillUserInfoViewModel.State.UserLoaded -> loadUi()
            is FillUserInfoViewModel.State.GoToExperience -> launchPreviousExperienceActivity()
            is FillUserInfoViewModel.State.ShowDatePicker -> showDatePicker()
            is FillUserInfoViewModel.State.UpdateBirthday -> setUpdatedBirthday()
            is FillUserInfoViewModel.State.EmptyFields -> ToastHelper.showToast(this, getString(R.string.str_fill_user_info_empty_fields))
            is FillUserInfoViewModel.State.InvalidEmail -> ToastHelper.showToast(this, getString(R.string.str_fill_user_info_invalid_email))
            is FillUserInfoViewModel.State.InvalidBirthday -> ToastHelper.showToast(this, getString(R.string.str_fill_user_info_invalid_birthday))
        }
    }

    private fun setUpdatedBirthday() {
        val birthdayEditText: EditText = findViewById(R.id.edittext_birthday)
        birthdayEditText.setText(viewModel.birthdayStr)
    }

    private fun launchPreviousExperienceActivity() {
        val intent = Intent(this, PreviousExperienceView::class.java)
        startActivity(intent)
    }

    private fun showDatePicker() {
        val datePickerFragment = DatePickerFragment(this, viewModel)
        datePickerFragment.show(supportFragmentManager, "datePicker")
    }

    class DatePickerFragment(private val view: FillUserInfoView, private val viewModel: FillUserInfoViewModel)
        : DialogFragment(), DatePickerDialog.OnDateSetListener {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val currDate = viewModel.birthday
            return DatePickerDialog(view, this,
                currDate.get(Calendar.YEAR),
                currDate.get(Calendar.MONTH),
                currDate.get(Calendar.DAY_OF_MONTH))
        }

        override fun onDateSet(view: DatePicker?, year: Int, month: Int, day: Int) {
            viewModel.birthday.set(year, month, day)
            viewModel.onBirthdayChanged()
        }
    }
}
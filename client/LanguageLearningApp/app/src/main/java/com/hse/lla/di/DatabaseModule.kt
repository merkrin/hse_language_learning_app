package com.hse.lla.di

import android.content.Context
import com.hse.lla.model.data.AppDatabase
import com.hse.lla.model.data.dao.*
import dagger.Module
import dagger.Provides

@Module
class DatabaseModule() {

    @Provides
    fun provideAppDatabase(context: Context): AppDatabase {
        return AppDatabase.getInstance(context)
    }

    @Provides
    fun provideLessonDao(appDatabase: AppDatabase): LessonDao {
        return appDatabase.lessonDao()
    }

    @Provides
    fun provideUserDao(appDatabase: AppDatabase): UserDao {
        return appDatabase.userDao()
    }

    @Provides
    fun provideTaskDao(appDatabase: AppDatabase): TaskDao {
        return appDatabase.taskDao()
    }

    @Provides
    fun provideUnitDao(appDatabase: AppDatabase): UnitDao {
        return appDatabase.unitDao()
    }

    @Provides
    fun provideCurriculumDao(appDatabase: AppDatabase): CurriculumDao {
        return appDatabase.curriculumDao()
    }

    @Provides
    fun provideFeedbackDao(appDatabase: AppDatabase): FeedbackDao {
        return appDatabase.feedbackDao()
    }

    @Provides
    fun provideUserProgressDao(appDatabase: AppDatabase): UserProgressDao {
        return appDatabase.userProgressDao()
    }

    @Provides
    fun provideApiTokenDao(appDatabase: AppDatabase): ApiTokenDao {
        return appDatabase.apiTokenDao()
    }
}
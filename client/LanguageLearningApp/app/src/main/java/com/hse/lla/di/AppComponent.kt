package com.hse.lla.di

import com.hse.lla.model.repo.CurriculumRepository
import com.hse.lla.model.repo.LessonRepository
import com.hse.lla.network.CurriculumDownloader
import com.hse.lla.viewmodel.*
import com.hse.lla.workers.PopulateDatabaseWorker
import dagger.Component


@Component(modules = [ApplicationModule::class, DatabaseModule::class])
interface AppComponent {
    fun inject(vm: LessonViewModel)
    fun inject(repository: LessonRepository)
    fun inject(vm: LessonsMapViewModel)
    fun inject(repository: CurriculumRepository)
    fun inject(worker: PopulateDatabaseWorker)
    fun inject(vm: FillUserInfoViewModel)
    fun inject(vm: UserProfileViewModel)
    fun inject(vm: RateUsViewModel)
    fun inject(vm: LanguageLevelViewModel)
    fun inject(vm: WelcomeViewModel)
    fun inject(vm: DownloadCurriculumViewModel)
    fun inject(downloader: CurriculumDownloader)
}
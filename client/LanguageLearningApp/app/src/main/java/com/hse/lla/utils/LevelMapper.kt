package com.hse.lla.utils

import com.hse.lla.R

class LevelMapper {
    companion object {
        private val levelIntToStringMap = mapOf(
            0 to "стартовый",
            1 to "начинающий",
            2 to "продвинутый",
            3 to "эксперт",
            4 to "профи"
        )

        private val levelIntToDrawableMap = mapOf(
            0 to R.drawable.level_icon_bicycle,
            1 to R.drawable.level_icon_car,
            2 to R.drawable.level_icon_train,
            3 to R.drawable.level_icon_plane,
            4 to R.drawable.level_icon_rocket
        )

        fun getLevelName(level: Int): String {
            return levelIntToStringMap[level] ?: ""
        }

        fun getLevelImage(level: Int): Int {
            return levelIntToDrawableMap[level] ?: R.drawable.level_icon_bicycle
        }
    }
}
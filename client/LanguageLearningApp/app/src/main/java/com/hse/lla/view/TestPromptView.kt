package com.hse.lla.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.hse.lla.R
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.IS_INITIAL_TEST
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.LESSON_ID

class TestPromptView : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_test_prompt)
    }

    fun onClick(v: View) {
        val intent = Intent(this, LessonController::class.java)
        intent.putExtra(IS_INITIAL_TEST, true)
        // TODO: нормально работать с тестом а не вот это
        intent.putExtra(LESSON_ID, 1)
        startActivity(intent)
    }
}
package com.hse.lla.model.repo

import com.hse.lla.model.data.entity.Lesson
import com.hse.lla.model.data.entity.LessonsUnit
import javax.inject.Inject

class ProgressUpdateHelper @Inject constructor(
    private val lessonRepository: LessonRepository,
    private val curriculumRepository: CurriculumRepository,
    private val userProgressRepository: UserProgressRepository
) {

    private var earnedMeatballs = 0

    fun getEarnedMeatballs() = earnedMeatballs

    suspend fun saveAll(lessonId: Long, correctAnswers: Int, totalTasks: Int) {
        saveLessonProgress(lessonId, correctAnswers, totalTasks)
        saveUnitProgress(lessonId)
        saveLevelProgress(lessonId)
        saveOverallProgress()
    }

    suspend fun saveLessonProgress(lessonId: Long, correctAnswers: Int, totalTasks: Int) {
        val lessonProgress = (correctAnswers.toDouble() / totalTasks * 100).toInt()
        earnedMeatballs = lessonProgress / 10

        lessonRepository.saveProgress(lessonId, lessonProgress)
        userProgressRepository.addMeatballs(earnedMeatballs)
    }

    suspend fun saveUnitProgress(lessonId: Long) {
        val unitId = lessonRepository.getUnit(lessonId)
        val lessons = curriculumRepository.getLessonsForUnit(unitId)
        val unitProgress = calculateUnitOrLevelProgress(lessons)

        curriculumRepository.saveUnitProgress(unitId, unitProgress)
    }

    suspend fun saveLevelProgress(lessonId: Long) {
        val level = lessonRepository.getLevel(lessonId)
        val lessons = curriculumRepository.getLessonsForLevel(level)
        val levelProgress = calculateUnitOrLevelProgress(lessons)

        curriculumRepository.saveLevelProgress(level, levelProgress)
    }

    suspend fun saveOverallProgress() {
//        val units = curriculumRepository.getUnits()
//        val progress = calculateOverallProgress(units)
        val progress = calculateAllLevelsProgress()
        userProgressRepository.setProgress(progress)
    }

    private fun calculateUnitOrLevelProgress(lessons: List<Lesson>): Int {
        var progress = 0.0
        val numLessons = lessons.size
        for (lesson in lessons) {
            progress += lesson.progress.toDouble() / numLessons
        }
        return progress.toInt()
    }

    private fun calculateOverallProgress(units: List<LessonsUnit>): Int {
        var progress = 0.0
        val numUnits = units.size
        for (unit in units) {
            progress += unit.progress.toDouble() / numUnits
        }
        return progress.toInt()
    }

    private suspend fun calculateAllLevelsProgress(): Int {
        var progress = 0.0
        for (i in 0 until CurriculumRepository.NUM_LEVELS) {
            progress += curriculumRepository.getLevelProgress(i).toDouble() /
                    CurriculumRepository.NUM_LEVELS
        }
        return progress.toInt()
    }
}
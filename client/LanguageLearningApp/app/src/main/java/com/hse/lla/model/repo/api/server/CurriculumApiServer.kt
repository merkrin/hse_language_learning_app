package com.hse.lla.model.repo.api.server

import com.hse.lla.network.data.request.TestResultsRequest
import com.hse.lla.network.data.response.CurriculumResponse
import retrofit2.Call
import retrofit2.http.*

interface CurriculumApiServer {
    @GET("api/get-lessons")
    @Headers("Content-Type: application/json", "Accept: application/json")
    fun getCurriculum(@Header("Authorization") token: String): Call<CurriculumResponse>

    @POST("api/test-results")
    @Headers("Content-Type: application/json", "Accept: application/json")
    fun postTestResults(@Header("Authorization") token: String,
                        @Body result: TestResultsRequest): Call<CurriculumResponse>
}
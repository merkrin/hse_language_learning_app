package com.hse.lla.view.constants

class IntentAndSharedPrefsConstants {
    companion object {
        const val FRW_COMPLETED = "frw_completed"
        const val IS_INITIAL_TEST = "is_initial_test"
        const val LESSON_ID = "lesson_id"
        const val INITIAL_TEST_TAKEN = "initial_test_taken"
        const val TOTAL_TASKS = "total_tasks"
        const val CORRECT_ANSWERS = "correct_answers"
        const val HAS_RATED = "has_rated"
        const val EARNED_MEATBALLS = "earned_meatballs"
        const val TOKEN = "api_token"
        const val IS_CURRICULUM_DOWNLOADED = "is_curriculum_downloaded"
    }
}
package com.hse.lla.model.repo

import com.hse.lla.model.data.dao.ApiTokenDao
import com.hse.lla.model.data.entity.User
import com.hse.lla.network.data.request.AuthorizeRequest
import com.hse.lla.network.data.response.TokenResponse
import com.hse.lla.model.repo.api.local.UserApiLocal
import com.hse.lla.model.repo.api.server.UserApiServer
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.util.*
import javax.inject.Inject

class UserRepository @Inject constructor(
    private val userApiLocal: UserApiLocal,
    private val userApiServer: UserApiServer,
    private val tokenDao: ApiTokenDao)
{
   // private val userApiServer = retrofit.create(UserApiServer::class.java)

    fun authorize(email: String) {
        // TODO: move out of repo?
        val call = userApiServer.authorize(AuthorizeRequest(email))
        call.enqueue(object: Callback<TokenResponse> {
            override fun onFailure(call: Call<TokenResponse>, t: Throwable) {
                println("request to server failed: $t")
            }

            override fun onResponse(call: Call<TokenResponse>, response: Response<TokenResponse>) {
                if (response.isSuccessful) {
                    val token = response.body()!!.token
                    runBlocking {
                        tokenDao.insert(token)
                    }
                    println("received token from server: $token")
                } else {
                    println("request to server failed: ${response.message()}")
                }
            }
        })
    }

    suspend fun getUser(): User {
        return userApiLocal.getUser()
    }

    suspend fun save(name: String, email: String, birthday: Calendar, city: String) {
        val user = User(0, name, email, birthday, city, 0)
        userApiLocal.insert(user)
    }

    suspend fun save(name: String, email: String) {
        save(name, email, Calendar.getInstance(), "")
    }
}
package com.hse.lla.model.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.hse.lla.model.data.entity.Lesson
import com.hse.lla.model.data.entity.Task

@Dao
interface LessonDao {
    @Query("SELECT * FROM tasks WHERE lesson_id = :currLessonId")
    suspend fun getTasks(currLessonId: Int): List<Task>

    @Query("SELECT COUNT(*) FROM tasks WHERE lesson_id = :currLessonId")
    suspend fun getTasksCount(currLessonId: Int): Int

    @Query("SELECT progress FROM lessons WHERE id = :lessonId")
    suspend fun getLessonProgress(lessonId: Long): Boolean

    @Query("UPDATE lessons SET progress=:lessonProgress WHERE id = :lessonId")
    suspend fun saveLessonProgress(lessonId: Long, lessonProgress: Int)

    @Query("SELECT unit FROM lessons WHERE id = :lessonId")
    suspend fun getUnit(lessonId: Long): Int

    @Query("SELECT difficulty FROM lessons WHERE id = :lessonId")
    suspend fun getLevel(lessonId: Long): Int

    @Insert
    suspend fun insert(lesson: Lesson): Long

    @Insert
    suspend fun insertAll(lessons: List<Lesson>)
}
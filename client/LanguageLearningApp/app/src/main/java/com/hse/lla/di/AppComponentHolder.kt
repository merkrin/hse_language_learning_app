package com.hse.lla.di

import android.content.Context
import java.lang.IllegalStateException

class AppComponentHolder {

    companion object {
        @Volatile private var instance: AppComponent? = null

        fun getComponent(): AppComponent {
            if (instance == null) {
                throw IllegalStateException("AppComponent is not inited!")
            }
            return instance!!
        }

        fun init(context: Context) {
            instance = DaggerAppComponent.builder()
                .applicationModule(ApplicationModule(context))
                .databaseModule(DatabaseModule())
                .build()
        }
    }
}
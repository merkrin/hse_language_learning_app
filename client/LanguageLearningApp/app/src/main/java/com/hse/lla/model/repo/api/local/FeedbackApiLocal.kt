package com.hse.lla.model.repo.api.local

import com.hse.lla.model.data.dao.FeedbackDao
import com.hse.lla.model.data.entity.Feedback
import javax.inject.Inject

class FeedbackApiLocal @Inject constructor(private val feedbackDao: FeedbackDao) {
    suspend fun insert(feedback: Feedback) {
        feedbackDao.insert(feedback)
    }
}
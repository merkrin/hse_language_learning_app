package com.hse.lla.model.repo.api.local

import com.hse.lla.model.data.dao.LessonDao
import com.hse.lla.model.data.dao.TaskDao
import com.hse.lla.model.data.entity.Task
import javax.inject.Inject

class LessonApiLocal @Inject constructor(
    private val lessonDao: LessonDao, private val taskDao: TaskDao
) {

    suspend fun getTasks(lessonId: Long): List<Task> {
        return taskDao.getTasksForLesson(lessonId)
    }

    suspend fun saveProgress(lessonId: Long, progress: Int) {
        lessonDao.saveLessonProgress(lessonId, progress)
    }

    suspend fun getUnit(lessonId: Long): Int {
        return lessonDao.getUnit(lessonId)
    }

    suspend fun getLevel(lessonId: Long): Int {
        return lessonDao.getLevel(lessonId)
    }
}
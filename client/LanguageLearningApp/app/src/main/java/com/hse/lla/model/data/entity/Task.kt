package com.hse.lla.model.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.hse.lla.model.data.entity.enums.TaskType

@Entity(tableName = "tasks")
data class Task(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "lesson_id") val lessonId: Long,
    @ColumnInfo(name = "type") val type: Int,
    @ColumnInfo(name = "question") val question: String,
    @ColumnInfo(name = "choices") val choices: List<String>?,
    @ColumnInfo(name = "answer") val answer: String,
    @ColumnInfo(name = "extra_text") val extraText: String?,
    @ColumnInfo(name = "extra_audio_path") val extraAudioPath: String?
)

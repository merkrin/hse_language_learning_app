package com.hse.lla.model.repo.api.server

import com.hse.lla.network.data.request.AuthorizeRequest
import com.hse.lla.network.data.response.TokenResponse
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface UserApiServer {
    @Headers("Content-Type: application/json")
    @POST("api/authorize")
    fun authorize(@Body authorizeRequest: AuthorizeRequest): Call<TokenResponse>
}
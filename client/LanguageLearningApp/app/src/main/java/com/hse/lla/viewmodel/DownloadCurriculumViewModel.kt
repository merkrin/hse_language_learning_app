package com.hse.lla.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hse.lla.model.repo.CurriculumRepository
import com.hse.lla.network.CurriculumDownloader
import javax.inject.Inject

class DownloadCurriculumViewModel: ViewModel() {

    private val mutableState = MutableLiveData<State>()
    val state: LiveData<State> = mutableState

    @Inject
    lateinit var curriculumRepository: CurriculumRepository

    private val listener = object : CurriculumDownloader.DownloadFinishedListener {
        override fun onDownloadFinished(success: Boolean) {
            if (success) {
                mutableState.postValue(State.DownloadFinished)
            } else {
                mutableState.postValue(State.DownloadFailed)
            }
        }
    }

    fun startDownload() {
        curriculumRepository.downloadCurriculum(listener)
        mutableState.postValue(State.DownloadStarted)
    }

    sealed class State {
        object DownloadStarted : State()
        object DownloadFinished : State()
        object DownloadFailed : State()
    }
}
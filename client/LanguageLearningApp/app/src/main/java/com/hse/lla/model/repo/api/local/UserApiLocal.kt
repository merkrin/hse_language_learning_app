package com.hse.lla.model.repo.api.local

import com.hse.lla.model.data.dao.UserDao
import com.hse.lla.model.data.entity.User
import javax.inject.Inject

class UserApiLocal @Inject constructor(private val userDao: UserDao) {
    suspend fun insert(user: User) {
        userDao.insert(user)
    }

    suspend fun getUser(): User {
        return userDao.get()
    }
}
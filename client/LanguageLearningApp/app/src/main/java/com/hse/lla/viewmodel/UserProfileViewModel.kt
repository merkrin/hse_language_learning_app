package com.hse.lla.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hse.lla.model.data.entity.User
import com.hse.lla.model.data.entity.UserProgress
import com.hse.lla.model.repo.UserProgressRepository
import com.hse.lla.model.repo.UserRepository
import com.hse.lla.utils.BirthdayValidator
import com.hse.lla.utils.StringUtils
import kotlinx.coroutines.launch
import java.util.*
import javax.inject.Inject

class UserProfileViewModel: ViewModel() {

    @Inject
    lateinit var userRepository: UserRepository

    @Inject
    lateinit var userProgressRepository: UserProgressRepository

    lateinit var user: User
    lateinit var userProgress: UserProgress

    private val mutableState = MutableLiveData<State>()
    val state: LiveData<State> = mutableState

    fun onInited() {
        viewModelScope.launch {
            user = userRepository.getUser()
            userProgress = userProgressRepository.get()
        }.invokeOnCompletion {
            mutableState.postValue(State.UserLoaded)
        }
    }

    fun onSave(name: String, email: String, city: String) {
        if (validate(name, email, city)) {
            viewModelScope.launch {
                userRepository.save(name, email, user.birthday!!, city)
                user = userRepository.getUser()
            }.invokeOnCompletion {
                mutableState.postValue(State.UserSaved)
            }
        }
    }

     private fun validate(name: String, email: String, city: String): Boolean {
        if (StringUtils.isEmpty(name)
            || StringUtils.isEmpty(email) || StringUtils.isEmpty(city)) {
            mutableState.postValue(State.EmptyFields)
            return false
        }
        if (!StringUtils.isValidEmail(email)) {
            mutableState.postValue(State.InvalidEmail)
            return false
        }
        if (!BirthdayValidator.isValidBirthday(user.birthday!!)) {
            mutableState.postValue(State.InvalidBirthday)
            return false
        }
        return true
    }

    sealed class State {
        object UserLoaded : State()
        object UserSaved : State()
        object EmptyFields : State()
        object InvalidEmail : State()
        object InvalidBirthday : State()
    }
}
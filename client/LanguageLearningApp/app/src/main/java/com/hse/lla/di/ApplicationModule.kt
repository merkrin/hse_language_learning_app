package com.hse.lla.di

import android.content.Context
import com.hse.lla.model.data.AppDatabase
import com.hse.lla.model.data.dao.*
import com.hse.lla.model.repo.*
import com.hse.lla.model.repo.api.local.*
import com.hse.lla.model.repo.api.server.CurriculumApiServer
import com.hse.lla.model.repo.api.server.UserApiServer
import com.hse.lla.network.CurriculumDownloader
import com.hse.lla.network.RetrofitClient
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

@Module
class ApplicationModule(val context: Context) {

    @Provides
    fun provideContext() = context

    @Provides
    fun provideLessonApiLocal(lessonDao: LessonDao, taskDao: TaskDao): LessonApiLocal {
        return LessonApiLocal(lessonDao, taskDao)
    }

    @Provides
    fun provideLessonRepository(lessonApiLocal: LessonApiLocal): LessonRepository {
        return LessonRepository(lessonApiLocal)
    }

    @Provides
    fun provideCurriculumApiLocal(curriculumDao: CurriculumDao, unitDao: UnitDao): CurriculumApiLocal {
        return CurriculumApiLocal(curriculumDao, unitDao)
    }

    @Provides
    fun provideCurriculumRepository(
        curriculumApiLocal: CurriculumApiLocal,
        curriculumDownloader: CurriculumDownloader): CurriculumRepository {
        return CurriculumRepository(curriculumApiLocal, curriculumDownloader)
    }

    @Provides
    fun provideRetrofitClient(): Retrofit {
        return RetrofitClient.getInstance()
    }

    @Provides
    fun provideUserApiLocal(userDao: UserDao): UserApiLocal {
        return UserApiLocal(userDao)
    }

    @Provides
    fun provideUserRepository(
        userApiLocal: UserApiLocal,
        userApiServer: UserApiServer,
        tokenDao: ApiTokenDao): UserRepository {
        return UserRepository(userApiLocal, userApiServer, tokenDao)
    }

    @Provides
    fun provideFeedbackRepository(feedbackApiLocal: FeedbackApiLocal): FeedbackRepository {
        return FeedbackRepository(feedbackApiLocal)
    }

    @Provides
    fun provideFeedbackApiLocal(feedbackDao: FeedbackDao): FeedbackApiLocal {
        return FeedbackApiLocal(feedbackDao)
    }

    @Provides
    fun provideUserProgressApiLocal(userProgressDao: UserProgressDao): UserProgressApiLocal {
        return UserProgressApiLocal(userProgressDao)
    }

    @Provides
    fun provideUserProgressRepository(userProgressApiLocal: UserProgressApiLocal): UserProgressRepository {
        return UserProgressRepository(userProgressApiLocal)
    }

    @Provides
    fun provideProgressUpdateHelper(
        lessonRepository: LessonRepository,
        curriculumRepository: CurriculumRepository,
        userProgressRepository: UserProgressRepository): ProgressUpdateHelper {
        return ProgressUpdateHelper(lessonRepository, curriculumRepository, userProgressRepository)
    }

    @Provides
    fun provideCurriculumApiServer(retrofit: Retrofit): CurriculumApiServer {
        return retrofit.create(CurriculumApiServer::class.java)
    }

    @Provides
    fun provideUserApiServer(retrofit: Retrofit): UserApiServer {
        return retrofit.create(UserApiServer::class.java)
    }

    @Provides
    fun provideCurriculumDownloader(
        database: AppDatabase,
        curriculumApiServer: CurriculumApiServer): CurriculumDownloader {
        return CurriculumDownloader(database, curriculumApiServer)
    }
}
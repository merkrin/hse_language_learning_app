package com.hse.lla.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import com.hse.lla.R
import com.hse.lla.model.data.entity.Lesson
import com.hse.lla.viewmodel.LessonsMapViewModel

class LessonsGridAdapter(
    private val context: Context,
    private val viewModel: LessonsMapViewModel
): BaseAdapter() {

    companion object {
        const val LESSON_PASSED_THRESHOLD = 50
    }

    private val lessons: List<Lesson> = viewModel.getLessons()

    override fun getCount(): Int = lessons.size

    override fun getItem(i: Int) = null

    override fun getItemId(i: Int): Long = 0

    override fun getView(i: Int, view: View?, viewGroup: ViewGroup?): View {
        return if (view == null) {
            val inflater: LayoutInflater =
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val lessonView = inflater.inflate(R.layout.view_lesson, null)
            lessonView.setOnClickListener {
                viewModel.onLessonSelected(lessons[i].id)
            }

            val lessonTitleView: TextView = lessonView.findViewById(R.id.textview_lesson_title)
            lessonTitleView.text = context.getString(R.string.str_lessons_map_lesson_title, lessons[i].id)

            val lessonIcon: ImageView = lessonView.findViewById(R.id.img_lesson)
            if (lessons[i].progress >= LESSON_PASSED_THRESHOLD) {
                lessonIcon.setImageDrawable(
                    AppCompatResources.getDrawable(
                        context,
                        R.drawable.lesson_icon_passed
                    )
                )
            }
            lessonView
        } else {
            view
        }
    }
}
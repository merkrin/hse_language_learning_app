package com.hse.lla.view

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.hse.lla.R
import com.hse.lla.di.AppComponentHolder
import com.hse.lla.utils.LevelMapper
import com.hse.lla.view.adapters.LessonsGridAdapter
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.IS_INITIAL_TEST
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.LESSON_ID
import com.hse.lla.viewmodel.LessonsMapViewModel
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.content.res.AppCompatResources


class LessonsMapView: AppCompatActivity() {
    private lateinit var viewModel: LessonsMapViewModel

    private lateinit var unitRecyclerView: RecyclerView

    private lateinit var lessonsGrid: GridView

    private lateinit var levelTitleTextView: TextView

    private lateinit var levelProgressTextView: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_lessons_map)

        viewModel = ViewModelProvider(this)
            .get(LessonsMapViewModel::class.java)
            .apply { state.observe(this@LessonsMapView, ::updateState) }

//        unitRecyclerView = findViewById(R.id.recycler_view_unit)
//        unitRecyclerView.layoutManager = LinearLayoutManager(this)
        lessonsGrid = findViewById(R.id.gridview_lessons)
        levelTitleTextView = findViewById(R.id.textview_level_title)
        levelProgressTextView = findViewById(R.id.textview_level_progress)

        val btnMenu: FloatingActionButton = findViewById(R.id.action_btn_to_menu)
        btnMenu.setOnClickListener {
            goToMenu()
        }

        AppComponentHolder.getComponent().inject(viewModel)
        viewModel.init()
    }

    override fun onResume() {
        super.onResume()
        viewModel.init()
    }

    private fun updateState(state: LessonsMapViewModel.State) {
        when (state) {
            is LessonsMapViewModel.State.OnInited -> displayCurriculum()
            is LessonsMapViewModel.State.GoToLesson -> goToLesson(state.lessonId)
            is LessonsMapViewModel.State.OnLevelCompleted -> showLevelCompletePopup()
        }
    }

    private fun displayCurriculum() {
       // unitRecyclerView.adapter = UnitsRecyclerViewAdapter(this, viewModel)
        lessonsGrid.adapter = LessonsGridAdapter(this, viewModel)
        levelTitleTextView.text = getString(R.string.str_lessons_map_level_title,
            viewModel.getLevel() + 1,
            LevelMapper.getLevelName(viewModel.getLevel()))
        levelProgressTextView.text = getString(R.string.str_lessons_map_level_progress,
            viewModel.getLevelProgress())
    }

    private fun goToLesson(lessonId: Long) {
        val intent = Intent(this, LessonController::class.java)
        intent.putExtra(LESSON_ID, lessonId)
        intent.putExtra(IS_INITIAL_TEST, false)
        startActivity(intent)
    }

    private fun goToMenu() {
        val intent = Intent(this, MenuView::class.java)
        startActivity(intent)
    }

    private fun showLevelCompletePopup() {
        val builder = AlertDialog.Builder(this, R.style.DialogTheme)
        builder.setTitle("Уровень пройден")
        val popupLayout: View = layoutInflater.inflate(R.layout.view_level_complete_popup, null)
        val btnOk: Button = popupLayout.findViewById(R.id.btn_ok_level_complete)

        val levelTextView: TextView = popupLayout.findViewById(R.id.textview_level_complete)
        val imgViewLevel: ImageView = popupLayout.findViewById(R.id.img_level)
        val currLevel = viewModel.getLevel()
        if (currLevel < 4) {
            val levelStr = LevelMapper.getLevelName(currLevel + 1)
            levelTextView.text = getString(R.string.str_lessons_map_level_complete, levelStr)
            val newImgId: Int = LevelMapper.getLevelImage(currLevel + 1)
            imgViewLevel.setImageDrawable(AppCompatResources.getDrawable(this, newImgId))
        } else {
            levelTextView.text = getString(R.string.str_lessons_map_all_levels_complete)
            imgViewLevel.setImageDrawable(AppCompatResources.getDrawable(this, R.drawable.star_icon))
        }

        builder.setView(popupLayout)

        val dialog = builder.create()
        btnOk.setOnClickListener {
            if (currLevel < 4) {
                viewModel.updateLevel()
            }
            dialog.dismiss()
        }
        dialog.show()
    }
}
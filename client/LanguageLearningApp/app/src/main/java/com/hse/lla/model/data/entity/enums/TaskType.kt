package com.hse.lla.model.data.entity.enums

enum class TaskType {
    MultipleChoice,
    MultipleChoiceAudio,
    MissingWord,
    MissingWordAudio;

    companion object {
        fun fromInt(value: Int) = values().first { it.ordinal == value }
    }
}
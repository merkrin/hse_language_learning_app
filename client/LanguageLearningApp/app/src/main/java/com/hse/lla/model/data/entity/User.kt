package com.hse.lla.model.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hse.lla.model.data.entity.enums.LanguageLevel
import java.util.*

@Entity
data class User(
    @PrimaryKey val uid: Int,
    @ColumnInfo(name = "user_name") val userName: String?,
    @ColumnInfo(name = "email") val email: String?,
    @ColumnInfo(name = "birthday") val birthday: Calendar?,
    @ColumnInfo(name = "city") val city: String?,
    @ColumnInfo(name = "language_level") val languageLevel: Int?
)

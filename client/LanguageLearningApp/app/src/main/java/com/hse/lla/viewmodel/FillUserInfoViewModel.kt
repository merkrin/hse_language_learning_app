package com.hse.lla.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hse.lla.model.data.entity.User
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import com.hse.lla.model.repo.UserRepository
import com.hse.lla.utils.BirthdayValidator
import com.hse.lla.utils.StringUtils
import kotlinx.coroutines.launch
import javax.inject.Inject


class FillUserInfoViewModel: ViewModel() {

    @Inject
    lateinit var userRepository: UserRepository

    private val mutableState = MutableLiveData<State>()
    val state: LiveData<State> = mutableState

    var name = MutableLiveData("")
    var email = MutableLiveData("")
    val city = MutableLiveData("")

    private lateinit var user: User

    private val dateFormat: DateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())

    var birthday: Calendar = Calendar.getInstance()
    var birthdayStr: String = dateFormat.format(birthday.time)

    fun loadUser() {
        viewModelScope.launch {
            user = userRepository.getUser()
        }.invokeOnCompletion {
            name = MutableLiveData(user.userName)
            email = MutableLiveData(user.email)
            mutableState.postValue(State.UserLoaded)
        }
    }

    fun onSave() {
        if (validate()) {
            viewModelScope.launch {
                userRepository.save(name.value!!, email.value!!, birthday, city.value!!)
            }.invokeOnCompletion {
                goNext()
            }
        }
    }

    fun onBirthdayEditTextClick() {
        mutableState.postValue(State.ShowDatePicker)
    }

    private fun validate(): Boolean {
        if (StringUtils.isEmpty(name.value!!)
            || StringUtils.isEmpty(email.value!!) || StringUtils.isEmpty(city.value!!)) {
            mutableState.postValue(State.EmptyFields)
            return false
        }
        if (!StringUtils.isValidEmail(email.value!!)) {
            mutableState.postValue(State.InvalidEmail)
            return false
        }
        if (!BirthdayValidator.isValidBirthday(birthday)) {
            mutableState.postValue(State.InvalidBirthday)
            return false
        }
        return true
    }

    private fun goNext() {
        mutableState.postValue(State.GoToExperience)
    }

    fun onBirthdayChanged() {
        birthdayStr = dateFormat.format(birthday.time)
        mutableState.postValue(State.UpdateBirthday)
    }

    sealed class State {
        object UserLoaded : State()
        object GoToExperience : State()
        object ShowDatePicker : State()
        object UpdateBirthday : State()
        object EmptyFields : State()
        object InvalidEmail : State()
        object InvalidBirthday : State()
    }
}
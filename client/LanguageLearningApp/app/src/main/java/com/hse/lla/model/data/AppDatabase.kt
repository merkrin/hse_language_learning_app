package com.hse.lla.model.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.workDataOf
import com.hse.lla.model.data.dao.*
import com.hse.lla.model.data.entity.*
import com.hse.lla.utils.Converters
import com.hse.lla.workers.PopulateDatabaseWorker
import com.hse.lla.workers.PopulateDatabaseWorker.Companion.LESSONS_FILENAME
import com.hse.lla.workers.PopulateDatabaseWorker.Companion.TASKS_FILENAME
import com.hse.lla.workers.PopulateDatabaseWorker.Companion.UNITS_FILENAME

@Database(entities = [User::class,
            Lesson::class,
            Task::class,
            LessonsUnit::class,
            Curriculum::class,
            Feedback::class,
            UserProgress::class,
            ApiToken::class,
            Level::class],
    version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class AppDatabase: RoomDatabase() {
    abstract fun userDao(): UserDao

    abstract fun lessonDao(): LessonDao

    abstract fun taskDao(): TaskDao

    abstract fun unitDao(): UnitDao

    abstract fun curriculumDao(): CurriculumDao

    abstract fun feedbackDao(): FeedbackDao

    abstract fun userProgressDao(): UserProgressDao

    abstract fun apiTokenDao(): ApiTokenDao

    companion object {
        private const val DATABASE_NAME = "language_learning_app_db"
        @Volatile private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
//                .addCallback(
//                    object : RoomDatabase.Callback() {
//                        override fun onCreate(db: SupportSQLiteDatabase) {
//                            super.onCreate(db)
//                            val request = OneTimeWorkRequestBuilder<PopulateDatabaseWorker>()
//                                .setInputData(workDataOf(TASKS_FILENAME to "tmp/tasks.json",
//                                    LESSONS_FILENAME to "tmp/lessons.json",
//                                    UNITS_FILENAME to "units.json"))
//                                .build()
//                            WorkManager.getInstance(context).enqueue(request)
//                        }
//                    }
//                )
                .build()
        }
    }
}
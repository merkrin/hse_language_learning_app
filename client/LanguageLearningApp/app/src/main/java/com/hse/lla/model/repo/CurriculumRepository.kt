package com.hse.lla.model.repo

import com.hse.lla.model.data.entity.Lesson
import com.hse.lla.model.data.entity.LessonsUnit
import com.hse.lla.model.data.entity.Level
import com.hse.lla.model.repo.api.local.CurriculumApiLocal
import com.hse.lla.network.CurriculumDownloader
import javax.inject.Inject

class CurriculumRepository @Inject constructor(
    private val curriculumApiLocal: CurriculumApiLocal,
    private val curriculumDownloader: CurriculumDownloader
) {
    companion object {
        const val NUM_LEVELS = 5
    }

    private var units: List<Unit>? = null

    fun downloadCurriculum(listener: CurriculumDownloader.DownloadFinishedListener) {
        curriculumDownloader.setListener(listener)
        curriculumDownloader.downloadCurriculum()
    }

    fun sendTestResults(correctAnswers: Int, totalTasks: Int) {
        val percent = (correctAnswers.toDouble() / totalTasks).toInt()
        curriculumDownloader.sendTestResult(percent)
    }

    suspend fun getUnits(): List<LessonsUnit> {
        return curriculumApiLocal.getUnits()
    }

    suspend fun getLessonsForUnit(unitId: Int): List<Lesson> {
        return curriculumApiLocal.getLessonsForUnit(unitId)
    }

    suspend fun getLessonsForLevel(level: Int): List<Lesson> {
        return curriculumApiLocal.getLessonsForLevel(level)
    }

    suspend fun saveUnitProgress(unitId: Int, progress: Int) {
        curriculumApiLocal.saveUnitProgress(unitId, progress)
    }

    suspend fun createLevels() {
        val levels = mutableListOf<Level>()
        for (i in 0 until NUM_LEVELS) {
            levels.add(Level(i, 0))
        }
        curriculumApiLocal.insert(levels)
    }

    suspend fun saveLevelProgress(level: Int, progress: Int) {
        curriculumApiLocal.saveLevelProgress(level, progress)
    }

    suspend fun getLevelProgress(level: Int): Int {
        return curriculumApiLocal.getLevelProgress(level)
    }
}
package com.hse.lla.utils

import java.util.*

class BirthdayValidator {
    companion object {
        private const val MIN_AGE = 12
        private const val EPOCH_YEAR = 1970

        fun isValidBirthday(birthday: Calendar): Boolean {
            val currDate = Calendar.getInstance()
            val diffInMs = currDate.timeInMillis - birthday.timeInMillis
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = diffInMs
            val diffInYears = calendar.get(Calendar.YEAR) - EPOCH_YEAR
            return diffInYears >= MIN_AGE
        }
    }
}
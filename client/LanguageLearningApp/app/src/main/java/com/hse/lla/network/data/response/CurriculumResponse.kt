package com.hse.lla.network.data.response

data class CurriculumResponse(
    val lessons: List<LessonWithProgressResponse>
)
package com.hse.lla.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

class RetrofitClient {
    companion object {
        // TODO: set proper url for emulator
        private const val BASE_URL = "http://localhost:8080"
        private const val TIMEOUT = 10

        @Volatile private var instance: Retrofit? = null

        fun getInstance(): Retrofit {
            return instance ?: synchronized(this) {
                instance ?: buildRetrofitClient()
            }
        }

        private fun buildRetrofitClient(): Retrofit {
            val interceptor = HttpLoggingInterceptor()
            interceptor.apply { interceptor.level = HttpLoggingInterceptor.Level.BODY }
            val okHttpClientBuilder = OkHttpClient
                .Builder()
                .addInterceptor(interceptor)
            okHttpClientBuilder.connectTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS)
            return Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClientBuilder.build())
                .build()
        }
    }
}
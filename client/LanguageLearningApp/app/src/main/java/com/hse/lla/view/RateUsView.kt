package com.hse.lla.view

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RatingBar
import androidx.lifecycle.ViewModelProvider
import com.hse.lla.R
import com.hse.lla.di.AppComponentHolder
import com.hse.lla.utils.ToastHelper
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.HAS_RATED
import com.hse.lla.viewmodel.RateUsViewModel

class RateUsView : AppCompatActivity() {
    private var prefs: SharedPreferences? = null

    private lateinit var viewModel: RateUsViewModel

    private lateinit var ratingBar: RatingBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_rate_us)

        prefs = getSharedPreferences("com.hse.lla", MODE_PRIVATE)

        viewModel = ViewModelProvider(this)
            .get(RateUsViewModel::class.java)
            .apply { state.observe(this@RateUsView, ::updateState) }
        AppComponentHolder.getComponent().inject(viewModel)

        ratingBar = findViewById(R.id.rating_bar)
        ratingBar.setOnRatingBarChangeListener { ratingBar, _, _ ->
            val btnSend: Button = findViewById(R.id.btn_send_rating)
            btnSend.isEnabled = true
            btnSend.setOnClickListener {
                val rating = ratingBar.rating.toInt()

                val commentEditText: EditText = findViewById(R.id.edittext_comment)
                val comment = commentEditText.text.toString()

                viewModel.onRatingSubmitted(rating, comment)
            }
        }

        val btnSkip: Button = findViewById(R.id.btn_skip_rating)
        btnSkip.setOnClickListener {
            finish()
        }
    }

    private fun updateState(state: RateUsViewModel.State) {
        when (state) {
            is RateUsViewModel.State.RatingSubmitted -> saveRatedAndFinish()
        }
    }

    private fun saveRatedAndFinish() {
        ToastHelper.showToast(this, getString(R.string.str_rate_us_done))
        prefs!!.edit().putBoolean(HAS_RATED, true).apply()
        finish()
    }
}
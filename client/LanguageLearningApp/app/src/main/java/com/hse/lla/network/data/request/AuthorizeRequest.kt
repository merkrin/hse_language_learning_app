package com.hse.lla.network.data.request

data class AuthorizeRequest(
    val email: String
)

package com.hse.lla.network.data.response

data class TaskResponse(
    val question: String,
    val type: String,
    val variants: List<String>,
    val fillGapAnswer: String?,
    val choiceTaskAnswers: List<Int>
)

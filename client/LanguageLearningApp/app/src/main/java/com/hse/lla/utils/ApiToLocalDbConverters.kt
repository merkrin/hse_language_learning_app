package com.hse.lla.utils

import com.hse.lla.model.data.entity.Lesson
import com.hse.lla.model.data.entity.Task
import com.hse.lla.model.data.entity.enums.TaskType
import com.hse.lla.network.data.response.LessonResponse
import com.hse.lla.network.data.response.TaskResponse

class ApiToLocalDbConverters {
    companion object {
        private const val MISSING_WORD_TYPE = "FILL_GAP"
        private const val MULTIPLE_CHOICE_TYPE = "CHOICE_TASK"

        fun toLocalTask(task: TaskResponse, lessonId: Long, taskId: Long): Task {
            var type: Int = 0
            var answer: String = ""
            if (task.type == MISSING_WORD_TYPE) {
                type = TaskType.MissingWord.ordinal
                answer = task.fillGapAnswer!!
            } else if (task.type == MULTIPLE_CHOICE_TYPE) {
                type = TaskType.MultipleChoice.ordinal
                answer = task.variants[task.choiceTaskAnswers[0]]
            }
            return Task(
                taskId,
                lessonId,
                type,
                task.question,
                task.variants,
                answer,
                "",
                ""
            )
        }

        fun toLocalLesson(lesson: LessonResponse, unitId: Int, lessonId: Long): Lesson {
            return Lesson(
                lessonId,
                lesson.taskCount,
                lesson.theory,
                lesson.difficulty - 1,
                unitId,
                0)
        }
    }
}
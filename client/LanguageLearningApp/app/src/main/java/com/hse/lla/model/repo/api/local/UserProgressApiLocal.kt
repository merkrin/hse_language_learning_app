package com.hse.lla.model.repo.api.local

import com.hse.lla.model.data.dao.UserProgressDao
import com.hse.lla.model.data.entity.UserProgress
import javax.inject.Inject

class UserProgressApiLocal @Inject constructor(
    private val userProgressDao: UserProgressDao
) {
    suspend fun get(): UserProgress {
        return userProgressDao.get()
    }

    suspend fun insert(userProgress: UserProgress) {
        return userProgressDao.insert(userProgress)
    }

    suspend fun getProgress(): Int {
        return userProgressDao.getProgress()
    }

    suspend fun setProgress(progress: Int) {
        userProgressDao.setProgress(progress)
    }

    suspend fun getMeatballs(): Int {
        return userProgressDao.getMeatballs()
    }

    suspend fun setMeatballs(meatballs: Int) {
        userProgressDao.setMeatballs(meatballs)
    }

    suspend fun getLevel(): Int {
        return userProgressDao.getLevel()
    }

    suspend fun setLevel(level: Int) {
        userProgressDao.setLevel(level)
    }
}
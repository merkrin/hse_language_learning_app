package com.hse.lla.model.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.hse.lla.model.data.entity.User

@Dao
interface UserDao {
    @Query("SELECT * FROM user")
    suspend fun get(): User

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(user: User)
}
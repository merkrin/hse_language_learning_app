package com.hse.lla.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.hse.lla.model.repo.UserRepository
import com.hse.lla.utils.GoogleSignInHelper
import kotlinx.coroutines.launch
import javax.inject.Inject

class WelcomeViewModel : ViewModel() {
    private val mutableState = MutableLiveData<State>()
    val state: LiveData<State> = mutableState

    @Inject
    lateinit var userRepository: UserRepository

    lateinit var googleSignInHelper: GoogleSignInHelper

    lateinit var userName: String
    lateinit var userEmail: String

    fun init() {
        googleSignInHelper.init()
    }

    fun onSignInClick() {
        if (!googleSignInHelper.isAlreadySignedIn()) {
            googleSignInHelper.signIn()
        } else {
            mutableState.postValue(State.SignedInSilent)
        }
    }

    private fun authorizeUser() {
        userRepository.authorize(userEmail)
    }

    fun handleSignIn(task: Task<GoogleSignInAccount>) {
        googleSignInHelper.handleSignIn(task)
        userName = googleSignInHelper.getName()!!
        userEmail = googleSignInHelper.getEmail()!!
        viewModelScope.launch {
            userRepository.save(userName, userEmail)
            authorizeUser()
        }.invokeOnCompletion {
            mutableState.postValue(State.SignedInFirst)
        }
    }

    sealed class State {
        object SignedInFirst : State()
        object SignedInSilent : State()
    }
}
package com.hse.lla.model.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.hse.lla.model.data.entity.Lesson
import com.hse.lla.model.data.entity.LessonsUnit

@Dao
interface UnitDao {
    @Query("SELECT * FROM lessons WHERE unit = :unitId")
    suspend fun getLessons(unitId: Int): List<Lesson>

    @Query("SELECT progress FROM units WHERE id = :unitId")
    suspend fun getProgress(unitId: Int): Int

    @Query("UPDATE units SET progress = :unitProgress WHERE id = :unitId")
    suspend fun saveProgress(unitId: Int, unitProgress: Int)

    @Insert
    suspend fun insert(unit: LessonsUnit)

    @Insert
    suspend fun insertAll(units: List<LessonsUnit>)
}
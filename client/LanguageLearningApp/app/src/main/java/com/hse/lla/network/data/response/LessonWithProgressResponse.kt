package com.hse.lla.network.data.response

data class LessonWithProgressResponse(
    val lesson: LessonResponse,
    val lessonProgress: LessonProgressResponse
)

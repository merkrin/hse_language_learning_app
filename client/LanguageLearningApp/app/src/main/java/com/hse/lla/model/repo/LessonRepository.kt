package com.hse.lla.model.repo

import com.hse.lla.model.data.entity.Task
import com.hse.lla.model.repo.api.local.LessonApiLocal
import javax.inject.Inject

class LessonRepository @Inject constructor(private val lessonApiLocal: LessonApiLocal) {
    private var tasksList: List<Task>? = null

    private var currTaskIndex = -1

    suspend fun getTasks(lessonId: Long): List<Task>? {
        tasksList = lessonApiLocal.getTasks(lessonId)
        return tasksList
    }

    suspend fun saveProgress(lessonId: Long, progress: Int) {
        lessonApiLocal.saveProgress(lessonId, progress)
    }

    suspend fun getUnit(lessonId: Long): Int {
        return lessonApiLocal.getUnit(lessonId)
    }

    suspend fun getLevel(lessonId: Long): Int {
        return lessonApiLocal.getLevel(lessonId)
    }

    fun checkAnswer(userAnswer: String, taskId: Long): Boolean {
        val currTask: Task? = tasksList!!.find { t ->  t.id == taskId }
        if (currTask != null) {
            return userAnswer == currTask.answer
        }
        return false
    }

    fun getNextTask(): Task? {
        ++currTaskIndex
        if (currTaskIndex < tasksList!!.size) {
            return tasksList!![currTaskIndex]
        }
        return null
    }
}
package com.hse.lla.utils

import android.text.TextUtils
import android.util.Patterns

class StringUtils {
    companion object {
        @JvmStatic
        fun isValidEmail(target: String): Boolean {
            return Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }

        @JvmStatic
        fun isEmpty(target: String): Boolean {
            return TextUtils.isEmpty(target)
        }
    }
}
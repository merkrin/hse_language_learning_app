package com.hse.lla.model.repo.api.local

import com.hse.lla.model.data.dao.CurriculumDao
import com.hse.lla.model.data.dao.UnitDao
import com.hse.lla.model.data.entity.Lesson
import com.hse.lla.model.data.entity.LessonsUnit
import com.hse.lla.model.data.entity.Level
import javax.inject.Inject

class CurriculumApiLocal @Inject constructor(private val curriculumDao: CurriculumDao,
                                             private val unitDao: UnitDao) {
    suspend fun getUserProgress(): Int {
        return curriculumDao.getUserProgress()
    }

    suspend fun getLessonsForUnit(unitId: Int): List<Lesson> {
        return unitDao.getLessons(unitId)
    }

    suspend fun getLessonsForLevel(level: Int): List<Lesson> {
        return curriculumDao.getLessonsForLevel(level)
    }

    suspend fun saveUnitProgress(unitId: Int, progress: Int) {
        unitDao.saveProgress(unitId, progress)
    }

    suspend fun getUnits(): List<LessonsUnit> {
        return curriculumDao.getUnits()
    }

    suspend fun saveLevelProgress(level: Int, progress: Int) {
        curriculumDao.saveLevelProgress(level, progress)
    }

    suspend fun getLevelProgress(level: Int): Int {
        return curriculumDao.getLevelProgress(level)
    }

    suspend fun insert(levels: List<Level>) {
        curriculumDao.insertLevels(levels)
    }
}
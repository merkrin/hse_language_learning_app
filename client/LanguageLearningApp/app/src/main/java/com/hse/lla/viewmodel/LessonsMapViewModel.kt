package com.hse.lla.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hse.lla.model.data.entity.Lesson
import com.hse.lla.model.data.entity.LessonsUnit
import com.hse.lla.model.repo.CurriculumRepository
import com.hse.lla.model.repo.UserProgressRepository
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.properties.Delegates

class LessonsMapViewModel: ViewModel() {

    companion object {
        const val LESSON_PASSED_THRESHOLD = 50
    }

    private val mutableState = MutableLiveData<State>()
    val state: LiveData<State> = mutableState

    private var units = listOf<LessonsUnit>()
    private var unitsToLessonsMap = mutableMapOf<Int, List<Lesson>>()

    private lateinit var lessons: List<Lesson>

    private var level = 0

    private var levelProgress = 0

    @Inject
    lateinit var curriculumRepository: CurriculumRepository

    @Inject
    lateinit var userProgressRepository: UserProgressRepository

    fun init() {
        viewModelScope.launch {
//            units = curriculumRepository.getUnits()
//            for (unit in units) {
//                val lessonsForUnit = curriculumRepository.getLessonsForUnit(unit.id)
//                unitsToLessonsMap[unit.id] = lessonsForUnit
//            }
            level = userProgressRepository.getLevel()
            levelProgress = curriculumRepository.getLevelProgress(level)
            lessons = curriculumRepository.getLessonsForLevel(level)
        }.invokeOnCompletion {
            if (checkLevelCompleted()) {
                mutableState.postValue(State.OnLevelCompleted)
            } else {
                mutableState.postValue(State.OnInited)
            }
        }
    }

    fun getUnits(): List<LessonsUnit> = units

    fun getLessons(): List<Lesson> = lessons

    fun getLevel(): Int = level

    fun getLevelProgress(): Int = levelProgress

    fun getLessonsForUnit(unitId: Int): List<Lesson> {
        return unitsToLessonsMap[unitId]!!
    }

    fun onLessonSelected(lessonId: Long) {
        mutableState.postValue(State.GoToLesson(lessonId))
    }

    fun updateLevel() {
        viewModelScope.launch {
            userProgressRepository.setLevel(++level)
            init()
        }
    }

    private fun checkLevelCompleted(): Boolean {
        for (lesson in lessons) {
            if (lesson.progress < LESSON_PASSED_THRESHOLD) {
                return false
            }
        }
        return true
    }

    sealed class State {
        data class GoToLesson(val lessonId: Long) : State()
        object OnInited : State()
        object OnLevelCompleted : State()
    }
}
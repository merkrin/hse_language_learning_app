package com.hse.lla.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class PreviousExperienceViewModel: ViewModel() {
    private val mutableState = MutableLiveData<State>()
    val state: LiveData<State> = mutableState

    fun onYes() {
        mutableState.postValue(State.GoToTest)
    }

    fun onNo() {
        mutableState.postValue(State.SkipTest)
    }

    sealed class State {
        object GoToTest : State()
        object SkipTest : State()
    }
}
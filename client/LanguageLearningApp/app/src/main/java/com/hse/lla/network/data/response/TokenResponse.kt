package com.hse.lla.network.data.response

data class TokenResponse(
    val token: String
)

package com.hse.lla.model.repo

import com.hse.lla.model.data.entity.Feedback
import com.hse.lla.model.repo.api.local.FeedbackApiLocal
import javax.inject.Inject

class FeedbackRepository @Inject constructor(private val feedbackApiLocal: FeedbackApiLocal) {
    suspend fun insertFeedback(feedback: Feedback) {
        feedbackApiLocal.insert(feedback)
    }
}
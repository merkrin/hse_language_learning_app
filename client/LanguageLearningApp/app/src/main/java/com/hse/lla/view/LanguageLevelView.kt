package com.hse.lla.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.content.res.AppCompatResources
import androidx.lifecycle.ViewModelProvider
import com.hse.lla.R
import com.hse.lla.di.AppComponentHolder
import com.hse.lla.utils.LevelMapper
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.CORRECT_ANSWERS
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.INITIAL_TEST_TAKEN
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.TOTAL_TASKS
import com.hse.lla.viewmodel.LanguageLevelViewModel

class LanguageLevelView : AppCompatActivity() {

    private lateinit var viewModel: LanguageLevelViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_language_level)

        viewModel = ViewModelProvider(this)
            .get(LanguageLevelViewModel::class.java)
            .apply { state.observe(this@LanguageLevelView, ::updateState) }
        AppComponentHolder.getComponent().inject(viewModel)
        viewModel.initUserProgress()
    }

    private fun updateState(state: LanguageLevelViewModel.State) {
        when (state) {
            is LanguageLevelViewModel.State.UserProgressCreated -> displayInfo()
        }
    }

    private fun displayInfo() {
        val initialTestTaken = intent.extras?.getBoolean(INITIAL_TEST_TAKEN)!!
        val langLevelTextView: TextView = findViewById(R.id.textview_language_level)
        if (!initialTestTaken) {
            langLevelTextView.text = getString(R.string.str_language_level_test_not_taken)
            viewModel.sendTestResults(0, 1)
        } else {
            val correctAnswers = intent.extras?.getInt(CORRECT_ANSWERS)
            val totalTasks = intent.extras?.getInt(TOTAL_TASKS)
            viewModel.sendTestResults(correctAnswers!!, totalTasks!!)
            val langLevel = viewModel.getLanguageLevel(correctAnswers!!, totalTasks!!)
            val langLevelStr = LevelMapper.getLevelName(langLevel)
            langLevelTextView.text = String.format(getString(R.string.str_language_level_test_result),
                correctAnswers, totalTasks, langLevelStr)

            val imgViewLevel: ImageView = findViewById(R.id.img_level)
            val drawable = LevelMapper.getLevelImage(langLevel)
            imgViewLevel.setImageDrawable(AppCompatResources.getDrawable(this, drawable))
        }

        val toMenuBtn: Button = findViewById(R.id.btn_finish_frw)
        toMenuBtn.setOnClickListener {
            goToDownloadCurriculum()
        }
    }

    private fun goToDownloadCurriculum() {
        val intent = Intent(this, DownloadCurriculumView::class.java)
//        val prefs = getSharedPreferences("com.hse.lla", MODE_PRIVATE)
//        prefs!!.edit().putBoolean(IntentAndSharedPrefsConstants.FRW_COMPLETED, true).apply()
//        val intent = Intent(this, LessonsMapView::class.java)
        startActivity(intent)
    }
}
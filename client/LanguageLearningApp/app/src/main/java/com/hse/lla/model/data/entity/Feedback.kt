package com.hse.lla.model.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "feedback")
data class Feedback(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo val rating: Int,
    @ColumnInfo val comment: String?
)

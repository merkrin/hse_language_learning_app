package com.hse.lla.model.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "curriculum")
data class Curriculum(
    @PrimaryKey val id: Int,
    @ColumnInfo val progress: Int
)

package com.hse.lla.network.data.response

data class LessonResponse(
    val tasks: List<TaskResponse>,
    val taskCount: Int,
    val theory: String,
    val difficulty: Int
)

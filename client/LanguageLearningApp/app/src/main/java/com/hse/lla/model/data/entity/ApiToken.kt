package com.hse.lla.model.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "api_token")
data class ApiToken(
    @PrimaryKey(autoGenerate = true) val id: Int,
    @ColumnInfo val token: String
)

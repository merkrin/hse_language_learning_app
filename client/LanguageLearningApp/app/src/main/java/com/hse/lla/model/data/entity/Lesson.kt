package com.hse.lla.model.data.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "lessons")
data class Lesson(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "task_count") val taskCount: Int,
    @ColumnInfo(name = "theory") val theory: String?,
    @ColumnInfo(name = "difficulty") val difficulty: Int,
    @ColumnInfo(name = "unit") val unit: Int,
    @ColumnInfo(name = "progress") val progress: Int
)
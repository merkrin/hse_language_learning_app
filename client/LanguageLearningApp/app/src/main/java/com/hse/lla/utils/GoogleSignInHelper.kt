package com.hse.lla.utils

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task

class GoogleSignInHelper(private val context: AppCompatActivity) {

    companion object {
        const val RC_SIGN_IN = 42
    }

    lateinit var options: GoogleSignInOptions
    lateinit var client: GoogleSignInClient
    lateinit var account: GoogleSignInAccount

    fun init() {
        options = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        client = GoogleSignIn.getClient(context, options)
    }

    fun isAlreadySignedIn(): Boolean {
        val account = GoogleSignIn.getLastSignedInAccount(context)
        return account != null
    }

    fun signIn() {
        val intent = client.signInIntent
        context.startActivityForResult(intent, RC_SIGN_IN)
    }

    fun handleSignIn(task: Task<GoogleSignInAccount>) {
        account = task.result
    }

    fun getName() = account.displayName

    fun getEmail() = account.email
}
package com.hse.lla.view

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.hse.lla.R
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.CORRECT_ANSWERS
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.EARNED_MEATBALLS
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.HAS_RATED
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.TOTAL_TASKS

class LessonResultView: AppCompatActivity() {
    private var prefs: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_lesson_result)
        prefs = getSharedPreferences("com.hse.lla", MODE_PRIVATE)

        val lessonResultTextView: TextView = findViewById(R.id.textview_lesson_result)
        val correctAnswers = intent.extras?.getInt(CORRECT_ANSWERS)
        val totalTasks = intent.extras?.getInt(TOTAL_TASKS)
        val meatballs = intent.extras?.getInt(EARNED_MEATBALLS)
        lessonResultTextView.text = getString(R.string.str_lesson_result,
            correctAnswers, totalTasks, meatballs)

        val btnToMenu: Button = findViewById(R.id.btn_to_menu)
        btnToMenu.setOnClickListener {
            finish()
            launchRatingActivityIfNeeded()
        }

        val btnToNextLesson: Button = findViewById(R.id.btn_to_next_lesson)
        btnToNextLesson.setOnClickListener {
            // TODO
        }
    }

    private fun launchRatingActivityIfNeeded() {
        if (!prefs!!.getBoolean(HAS_RATED, false)) {
            val intent = Intent(this, RateUsView::class.java)
            startActivity(intent)
        }
    }
}
package com.hse.lla.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hse.lla.model.data.entity.enums.LanguageLevel
import com.hse.lla.model.repo.CurriculumRepository
import com.hse.lla.model.repo.UserProgressRepository
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class LanguageLevelViewModel: ViewModel() {

    private val mutableState = MutableLiveData<State>()
    val state: LiveData<State> = mutableState

    @Inject
    lateinit var userProgressRepository: UserProgressRepository

    @Inject
    lateinit var curriculumRepository: CurriculumRepository

    fun initUserProgress() {
        viewModelScope.launch {
            curriculumRepository.createLevels()
            userProgressRepository.create(LanguageLevel.Beginner.ordinal)
        }.invokeOnCompletion {
            mutableState.postValue(State.UserProgressCreated)
        }
    }

    fun getLanguageLevel(correctAnswers: Int, totalTasks: Int): Int {
        var level: Int
        runBlocking {
            level = userProgressRepository
                        .calculateAndSetLanguageLevel(correctAnswers, totalTasks)
        }
        return level
    }

    fun sendTestResults(correctAnswers: Int, totalTasks: Int) {
        viewModelScope.launch {
            curriculumRepository.sendTestResults(correctAnswers, totalTasks)
        }
    }

    sealed class State {
        object UserProgressCreated : State()
    }
}
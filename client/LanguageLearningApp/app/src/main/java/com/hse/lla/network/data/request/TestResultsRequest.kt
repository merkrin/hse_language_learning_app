package com.hse.lla.network.data.request

data class TestResultsRequest(
    val percent: Int
)

package com.hse.lla.model.data.entity.enums

enum class LanguageLevel {
    Beginner, Rookie, Independent, Expert, Pro;

    companion object {
        fun fromInt(value: Int) = values().first { it.ordinal == value }
    }
}
package com.hse.lla.network

import android.util.Log
import com.hse.lla.di.AppComponentHolder
import com.hse.lla.model.data.AppDatabase
import com.hse.lla.model.data.entity.Lesson
import com.hse.lla.model.data.entity.Task
import com.hse.lla.model.repo.api.server.CurriculumApiServer
import com.hse.lla.network.data.request.TestResultsRequest
import com.hse.lla.network.data.response.CurriculumResponse
import com.hse.lla.utils.ApiToLocalDbConverters
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

class CurriculumDownloader @Inject constructor(
    private val database: AppDatabase,
    private val curriculumApiServer: CurriculumApiServer
) {
    private val dispatcher = Dispatchers.IO

    private lateinit var listener: DownloadFinishedListener

    private lateinit var token: String

    init {
        AppComponentHolder.getComponent().inject(this)
        runBlocking {
            getToken()
        }
    }

    fun setListener(listener: DownloadFinishedListener) {
        this.listener = listener
    }

    private fun notifyListener(success: Boolean) {
        listener.onDownloadFinished(success)
    }

    private suspend fun getToken() {
        token = database.apiTokenDao().get()
    }

    fun downloadCurriculum() {
        val call = curriculumApiServer.getCurriculum(token)
        call.enqueue(object: Callback<CurriculumResponse> {
            override fun onResponse(
                call: Call<CurriculumResponse>,
                response: Response<CurriculumResponse>
            ) {
                if (response.isSuccessful) {
                    val curriculumResponse = response.body()
                    val scope = CoroutineScope(dispatcher)
                    scope.launch {
                        parseAndSaveCurriculumResponse(curriculumResponse!!)
                    }.invokeOnCompletion {
                        notifyListener(true)
                    }
                } else {
                    notifyListener(false)
                }
            }

            override fun onFailure(call: Call<CurriculumResponse>, t: Throwable) {
                Log.e(TAG, "failed to get curriculum: $t")
                notifyListener(false)
            }
        })
    }

    fun sendTestResult(percent: Int) {
        val call = curriculumApiServer.postTestResults(token, TestResultsRequest(percent))
        call.enqueue(object: Callback<CurriculumResponse> {
            override fun onResponse(
                call: Call<CurriculumResponse>,
                response: Response<CurriculumResponse>
            ) {
                if (response.isSuccessful) {
                    Log.i(TAG, "posted test results")
                } else {
                    Log.e(TAG, "failed to post test results")
                }
            }

            override fun onFailure(call: Call<CurriculumResponse>, t: Throwable) {
                Log.e(TAG, "failed to post test results")
            }
        })
    }

    private suspend fun parseAndSaveCurriculumResponse(curriculumResponse: CurriculumResponse) {
        val lessons = curriculumResponse.lessons
        //val lessonsProgress = curriculumResponse.lessonsProgress
        val currUnit = 1

        val localLessons = mutableListOf<Lesson>()
        val localTasks = mutableListOf<Task>()

        var currLesson = 0L
        var currTask = 0L
        for (lesson in lessons) {
            val lessonLocal = ApiToLocalDbConverters.toLocalLesson(lesson.lesson, currUnit, currLesson)
            localLessons.add(lessonLocal)

            val tasks = lesson.lesson.tasks
            for (task in tasks) {
                val taskLocal = ApiToLocalDbConverters.toLocalTask(task, currLesson, currTask)
                ++currTask
                localTasks.add(taskLocal)
            }
            ++currLesson
        }
        saveLessonsAndTasks(localLessons, localTasks)
    }

    private suspend fun saveLessonsAndTasks(lessons: List<Lesson>, tasks: List<Task>) {
        database.lessonDao().insertAll(lessons)
        database.taskDao().insertAll(tasks)
    }

    companion object {
        private val TAG = "CurriculumDownloader"
    }

    interface DownloadFinishedListener {
        fun onDownloadFinished(success: Boolean)
    }
}
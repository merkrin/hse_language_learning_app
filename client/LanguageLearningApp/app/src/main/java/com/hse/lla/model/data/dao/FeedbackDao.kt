package com.hse.lla.model.data.dao

import androidx.room.Dao
import androidx.room.Insert
import com.hse.lla.model.data.entity.Feedback

@Dao
interface FeedbackDao {
    @Insert
    suspend fun insert(feedback: Feedback)
}
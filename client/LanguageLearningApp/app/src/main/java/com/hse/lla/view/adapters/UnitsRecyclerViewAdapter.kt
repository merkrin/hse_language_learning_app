package com.hse.lla.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.hse.lla.R
import com.hse.lla.viewmodel.LessonsMapViewModel

class UnitsRecyclerViewAdapter(private val context: Context,
            private val viewModel: LessonsMapViewModel):
    RecyclerView.Adapter<UnitsRecyclerViewAdapter.ViewHolder>()
{
    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val unitView: View = view.findViewById(R.id.layout_unit)
        val unitTitle: TextView = view.findViewById(R.id.textview_unit_title)
        val unitProgress: TextView = view.findViewById(R.id.textview_unit_progress)
        val lessonsGrid: GridView = view.findViewById(R.id.gridview_lessons)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.view_unit, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val currUnit = viewModel.getUnits()[position]
       // holder.lessonsGrid.adapter = LessonsGridAdapter(context, viewModel, currUnit.id)
        holder.unitTitle.text = currUnit.title
        holder.unitProgress.text = context.getString(R.string.str_lessons_map_unit_progress, currUnit.progress)
    }

    override fun getItemCount(): Int = viewModel.getUnits().size
}
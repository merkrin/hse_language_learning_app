package com.hse.lla.view

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.GridView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.hse.lla.R
import com.hse.lla.databinding.ViewGeneralTaskBinding
import com.hse.lla.di.AppComponentHolder
import com.hse.lla.model.data.entity.enums.TaskType
import com.hse.lla.view.adapters.ButtonsGridAdapter
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.CORRECT_ANSWERS
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.EARNED_MEATBALLS
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.INITIAL_TEST_TAKEN
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.IS_INITIAL_TEST
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.LESSON_ID
import com.hse.lla.view.constants.IntentAndSharedPrefsConstants.Companion.TOTAL_TASKS
import com.hse.lla.viewmodel.LessonViewModel

class LessonController: AppCompatActivity() {
    private lateinit var viewModel: LessonViewModel

    private lateinit var binding: ViewGeneralTaskBinding

    private var firstTask = true

    private lateinit var missingWordEditText: EditText
    private lateinit var choicesGridView: GridView
    private lateinit var dynamicViewsList: List<View>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.view_general_task)
        binding.lifecycleOwner = this

        viewModel = ViewModelProvider(this)
            .get(LessonViewModel::class.java)
            .apply { state.observe(this@LessonController, ::updateState) }
        viewModel.isInitialTest = intent.extras?.getBoolean(IS_INITIAL_TEST) ?: false
        viewModel.lessonId = intent.extras?.getLong(LESSON_ID) ?: 0L

        AppComponentHolder.getComponent().inject(viewModel)
        viewModel.onInited()
        initDynamicViews()
        binding.lessonViewModel = viewModel
    }

    private fun initDynamicViews() {
        missingWordEditText = findViewById(R.id.edittext_missing_word)
        choicesGridView = findViewById(R.id.gridview_options)

        dynamicViewsList = listOf(missingWordEditText, choicesGridView)
    }

    private fun resetBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.view_general_task)
        binding.lifecycleOwner = this
        binding.lessonViewModel = viewModel
        initDynamicViews()
    }

    private fun updateState(state: LessonViewModel.State) {
        when (state) {
            is LessonViewModel.State.GoToNextQuestion -> openNextTask(state.taskType)
            is LessonViewModel.State.GoToResult -> openTaskResult(state.correctAnswer)
            is LessonViewModel.State.GoToLessonResult -> openLessonOrTestResult(false, state.totalTasks, state.correctAnswers, state.earnedMeatballs)
            is LessonViewModel.State.GoToTestResult -> openLessonOrTestResult(true, state.totalTasks, state.correctAnswers, 0)
        }
    }

    private fun setViewVisibility(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.INVISIBLE
    }

    private fun setVisibilityForTask(taskType: TaskType) {
        when (taskType) {
            TaskType.MultipleChoice -> setViewVisibility(choicesGridView, true)
            TaskType.MultipleChoiceAudio -> setViewVisibility(choicesGridView, true)
            TaskType.MissingWord -> setViewVisibility(missingWordEditText, true)
            TaskType.MissingWordAudio -> setViewVisibility(missingWordEditText, true)
        }
    }

    private fun setDynamicViewsInvisible() {
        for (view in dynamicViewsList) {
            setViewVisibility(view, false)
        }
    }

    private fun openNextTask(taskType: TaskType) {
        if (firstTask) {
            firstTask = false
            // урааа костыли
            val questionTextView: TextView = findViewById(R.id.textview_task_question)
            questionTextView.text = viewModel.currTask!!.question
        } else {
            resetBinding()
        }
        setDynamicViewsInvisible()
        setVisibilityForTask(taskType)
        choicesGridView.adapter = ButtonsGridAdapter(this, viewModel)

        val submitBtn: Button = findViewById(R.id.btn_submit)
        if (taskType == TaskType.MultipleChoice || taskType == TaskType.MultipleChoiceAudio) {
            setViewVisibility(submitBtn, false)
        } else {
            submitBtn.setOnClickListener {
                viewModel.onAnswerSubmitted()
            }
        }
    }

    private fun openTaskResult(result: Boolean) {
        setContentView(R.layout.view_task_result)
        val resultTextView: TextView = findViewById(R.id.textview_task_result)
        resultTextView.text = if (result) {
            getString(R.string.str_task_result_correct)
        } else {
            getString(R.string.str_task_result_incorrect)
        }

        val goNextBtn: Button = findViewById(R.id.btn_go_next)
        goNextBtn.setOnClickListener {
            viewModel.goToNextQuestion()
        }
    }

    private fun openLessonOrTestResult(isTest: Boolean, totalTasks: Int, correctAnswers: Int, earnedMeatballs: Int) {
        finish()
        val cls = if (isTest) LanguageLevelView::class.java else LessonResultView::class.java
        val intent = Intent(this, cls)
        intent.putExtra(TOTAL_TASKS, totalTasks)
        intent.putExtra(CORRECT_ANSWERS, correctAnswers)
        if (isTest) {
            intent.putExtra(INITIAL_TEST_TAKEN, true)
        } else {
            intent.putExtra(EARNED_MEATBALLS, earnedMeatballs)
        }
        startActivity(intent)
    }
}
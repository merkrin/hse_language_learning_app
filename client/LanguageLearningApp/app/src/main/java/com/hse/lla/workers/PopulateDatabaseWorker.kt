package com.hse.lla.workers

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.google.gson.stream.JsonReader
import com.hse.lla.di.AppComponentHolder
import com.hse.lla.model.data.AppDatabase
import com.hse.lla.model.data.entity.Lesson
import com.hse.lla.model.data.entity.LessonsUnit
import com.hse.lla.model.data.entity.Task
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

// TODO: remove or refactor to get data from server
class PopulateDatabaseWorker(
    context: Context,
    workerParams: WorkerParameters
) : CoroutineWorker(context, workerParams)
{

    @Inject
    lateinit var database: AppDatabase

    init {
        AppComponentHolder.getComponent().inject(this)
    }

    override suspend fun doWork(): Result = withContext(Dispatchers.IO) {
        try {
            val tasksFilename = inputData.getString(TASKS_FILENAME)
            if (tasksFilename != null) {
                applicationContext.assets.open(tasksFilename).use { inputStream ->
                    JsonReader(inputStream.reader()).use { jsonReader ->
                        val taskType = object : TypeToken<List<Task>>() {}.type
                        val tasksList: List<Task> = Gson().fromJson(jsonReader, taskType)

                        database.taskDao().insertAll(tasksList)
                        Log.i(TAG, "successfully inserted tasks!")

                        Result.success()
                    }
                }
            } else {
                Log.e(TAG, "Error seeding database (tasks) - no valid filename")
                Result.failure()
            }

            val lessonsFilename = inputData.getString(LESSONS_FILENAME)
            if (lessonsFilename != null) {
                applicationContext.assets.open(lessonsFilename).use { inputStream ->
                    JsonReader(inputStream.reader()).use { jsonReader ->
                        val lessonType = object : TypeToken<List<Lesson>>() {}.type
                        val lessonsList: List<Lesson> = Gson().fromJson(jsonReader, lessonType)

                        database.lessonDao().insertAll(lessonsList)
                        Log.i(TAG, "successfully inserted lessons!!")

                        Result.success()
                    }
                }
            } else {
                Log.e(TAG, "Error seeding database (lessons) - no valid filename")
                Result.failure()
            }

            val unitsFilename = inputData.getString(UNITS_FILENAME)
            if (unitsFilename != null) {
                applicationContext.assets.open(unitsFilename).use { inputStream ->
                    JsonReader(inputStream.reader()).use { jsonReader ->
                        val unitType = object : TypeToken<List<LessonsUnit>>() {}.type
                        val unitsList: List<LessonsUnit> = Gson().fromJson(jsonReader, unitType)

                        database.unitDao().insertAll(unitsList)
                        Log.i(TAG, "successfully inserted units!")

                        Result.success()
                    }
                }
            } else {
                Log.e(TAG, "Error seeding database (units) - no valid filename")
                Result.failure()
            }
        } catch (ex: Exception) {
            Log.e(TAG, "Error seeding database", ex)
            Result.failure()
        }
    }



    companion object {
        private const val TAG = "PopulateDatabaseWorker"
        const val TASKS_FILENAME = "TASKS_DATA_FILENAME"
        const val LESSONS_FILENAME = "LESSONS_DATA_FILENAME"
        const val UNITS_FILENAME = "UNITS_DATA_FILENAME"
    }
}
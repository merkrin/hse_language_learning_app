package com.hse.lla.view

import android.app.DatePickerDialog
import android.app.Dialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.content.res.AppCompatResources
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.hse.lla.R
import com.hse.lla.di.AppComponentHolder
import com.hse.lla.utils.LevelMapper
import com.hse.lla.utils.ToastHelper
import com.hse.lla.viewmodel.UserProfileViewModel
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class UserProfileView : AppCompatActivity() {
    private lateinit var viewModel: UserProfileViewModel

    private lateinit var userNameEditText: EditText
    private lateinit var userEmailEditText: EditText
    private lateinit var userBirthdayEditText: EditText
    private lateinit var userCityEditText: EditText

    private lateinit var editableViews: List<View>

    private lateinit var btnEdit: Button
    private lateinit var btnSave: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.view_user_profile)

        initViews()

        viewModel = ViewModelProvider(this)
            .get(UserProfileViewModel::class.java)
            .apply { state.observe(this@UserProfileView, ::updateState) }
        AppComponentHolder.getComponent().inject(viewModel)
        viewModel.onInited()
    }

    private fun initViews() {
        userNameEditText = findViewById(R.id.edittext_user_name)
        userEmailEditText = findViewById(R.id.edittext_user_email)
        userBirthdayEditText = findViewById(R.id.edittext_user_birthday)
        userCityEditText = findViewById(R.id.edittext_user_city)

        userBirthdayEditText.setOnClickListener {
            if (userBirthdayEditText.isEnabled) {
                showDatePicker()
            }
        }

        editableViews = listOf(userNameEditText,
            userEmailEditText,
            userBirthdayEditText,
            userCityEditText)

        btnEdit = findViewById(R.id.btn_edit)
        btnEdit.setOnClickListener {
            btnEdit.visibility = View.INVISIBLE
            btnSave.visibility = View.VISIBLE
            enableViews(true)
        }

        btnSave = findViewById(R.id.btn_save_profile)
        btnSave.setOnClickListener {
            viewModel.onSave(userNameEditText.text.toString(),
                userEmailEditText.text.toString(),
                userCityEditText.text.toString())
        }
    }

    private fun enableViews(enabled: Boolean) {
        for (view in editableViews) {
            view.isEnabled = enabled
        }
    }

    private fun updateState(state: UserProfileViewModel.State) {
        when (state) {
            is UserProfileViewModel.State.UserLoaded -> displayUserInfo()
            is UserProfileViewModel.State.EmptyFields -> ToastHelper.showToast(this, getString(R.string.str_fill_user_info_empty_fields))
            is UserProfileViewModel.State.InvalidBirthday -> ToastHelper.showToast(this, getString(R.string.str_fill_user_info_invalid_birthday))
            is UserProfileViewModel.State.InvalidEmail -> ToastHelper.showToast(this, getString(R.string.str_fill_user_info_invalid_email))
            is UserProfileViewModel.State.UserSaved -> {
                ToastHelper.showToast(this, getString(R.string.str_user_profile_user_saved))
                enableViews(false)
                btnEdit.visibility = View.VISIBLE
                btnSave.visibility = View.INVISIBLE
                displayUserInfo()
            }
        }
    }

    private fun displayUserInfo() {
        val user = viewModel.user
        userNameEditText.setText(user.userName)
        userEmailEditText.setText(user.email)

        displayBirthday()

        userCityEditText.setText(user.city)

        displayUserProgress()
    }

    private fun displayUserProgress() {
        val userProgress = viewModel.userProgress

        val progressTextView: TextView = findViewById(R.id.textview_user_progress)
        progressTextView.text = getString(R.string.str_user_profile_progress, userProgress.overallProgress)

        val levelTextView: TextView = findViewById(R.id.textview_user_language_level)
        levelTextView.text = getString(R.string.str_user_profile_level,
            userProgress.level + 1,
            LevelMapper.getLevelName(userProgress.level))

        val imgViewLevel: ImageView = findViewById(R.id.img_level)
        val drawable = LevelMapper.getLevelImage(userProgress.level)
        imgViewLevel.setImageDrawable(AppCompatResources.getDrawable(this, drawable))

        val meatballsTextView: TextView = findViewById(R.id.textview_meatballs)
        meatballsTextView.text = getString(R.string.str_user_profile_meatballs_count, userProgress.numMeatballs)
    }

    private fun displayBirthday() {
        val dateFormat: DateFormat = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault())
        userBirthdayEditText.setText(dateFormat.format(viewModel.user.birthday!!.time))
    }

    private fun showDatePicker() {
        val datePickerFragment = DatePickerFragment(this, viewModel)
        datePickerFragment.show(supportFragmentManager, "datePicker")
    }

    class DatePickerFragment(private val view: UserProfileView, private val viewModel: UserProfileViewModel)
        : DialogFragment(), DatePickerDialog.OnDateSetListener {

        override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
            val currDate = viewModel.user.birthday
            return DatePickerDialog(view, this,
                currDate!!.get(Calendar.YEAR),
                currDate.get(Calendar.MONTH),
                currDate.get(Calendar.DAY_OF_MONTH))
        }

        override fun onDateSet(picker: DatePicker?, year: Int, month: Int, day: Int) {
            viewModel.user.birthday!!.set(year, month, day)
            view.displayBirthday()
        }
    }
}
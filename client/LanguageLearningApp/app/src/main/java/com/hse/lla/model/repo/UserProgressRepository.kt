package com.hse.lla.model.repo

import com.hse.lla.model.data.entity.UserProgress
import com.hse.lla.model.repo.api.local.UserProgressApiLocal
import javax.inject.Inject

class UserProgressRepository @Inject constructor(
    private val userProgressApiLocal: UserProgressApiLocal
) {

    companion object {
        const val THRESHOLD = 0.5
    }

    suspend fun get(): UserProgress {
        return userProgressApiLocal.get()
    }

    suspend fun create(userProgress: UserProgress) {
        userProgressApiLocal.insert(userProgress)
    }

    suspend fun create(level: Int) {
        create(UserProgress(0, 0, 0, level))
    }

    suspend fun getProgress(): Int {
        return userProgressApiLocal.getProgress()
    }

    suspend fun setProgress(progress: Int) {
        userProgressApiLocal.setProgress(progress)
    }

    suspend fun getMeatballs(): Int {
        return userProgressApiLocal.getMeatballs()
    }

    suspend fun addMeatballs(meatballs: Int) {
        val currMeatballs = userProgressApiLocal.getMeatballs()
        userProgressApiLocal.setMeatballs(currMeatballs + meatballs)
    }

    suspend fun subtractMeatballs(meatballs: Int): Boolean {
        val currMeatballs = userProgressApiLocal.getMeatballs()
        if (currMeatballs < meatballs) {
            return false
        }
        userProgressApiLocal.setMeatballs(currMeatballs - meatballs)
        return true
    }

    suspend fun getLevel(): Int {
        return userProgressApiLocal.getLevel()
    }

    suspend fun setLevel(level: Int) {
        userProgressApiLocal.setLevel(level)
    }

    suspend fun calculateAndSetLanguageLevel(correctAnswers: Int, totalTasks: Int): Int {
        val percentage: Double = correctAnswers.toDouble() / totalTasks
        val level = getLevelFromPercent(percentage)
        create(level)
        return level
    }

    private fun getLevelFromPercent(percent: Double): Int {
        if (percent < 0.2) return 0
        if (percent < 0.5) return 1
        if (percent < 0.7) return 2
        if (percent < 0.9) return 3
        return 4
    }
}
package com.hse.lla.model.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.hse.lla.model.data.entity.Task

@Dao
interface TaskDao {
    @Query("SELECT * FROM tasks WHERE lesson_id = :currLessonId")
    suspend fun getTasksForLesson(currLessonId: Long): List<Task>

    @Query("SELECT * FROM tasks")
    suspend fun getAllTasks(): List<Task>

    @Insert
    suspend fun insert(task: Task)

    @Insert
    suspend fun insertAll(tasks: List<Task>)
}
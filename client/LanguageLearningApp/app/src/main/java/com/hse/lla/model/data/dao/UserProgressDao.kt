package com.hse.lla.model.data.dao

import androidx.room.*
import com.hse.lla.model.data.entity.UserProgress

@Dao
interface UserProgressDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(userProgress: UserProgress)

    @Update
    suspend fun update(userProgress: UserProgress)

    @Query("SELECT * FROM user_progress LIMIT 1")
    suspend fun get(): UserProgress

    @Query("SELECT overallProgress FROM user_progress LIMIT 1")
    suspend fun getProgress(): Int

    @Query("UPDATE user_progress SET overallProgress = :progress")
    suspend fun setProgress(progress: Int)

    @Query("SELECT numMeatballs FROM user_progress LIMIT 1")
    suspend fun getMeatballs(): Int

    @Query("UPDATE user_progress SET numMeatballs = :meatballs")
    suspend fun setMeatballs(meatballs: Int)

    @Query("SELECT level FROM user_progress LIMIT 1")
    suspend fun getLevel(): Int

    @Query("UPDATE user_progress SET level = :langLevel")
    suspend fun setLevel(langLevel: Int)
}
INSERT INTO feedback VALUES (1, 5, NULL, current_timestamp);

CREATE TABLE IF NOT EXISTS temp_lessons_ids (lesson_name VARCHAR, lesson_id BIGINT);
--УРОКИ
WITH lesson1_id AS (
    INSERT INTO lessons (name, difficulty, theory)
        VALUES ('Buying things', 0, 'Lesson 1')
        RETURNING id) --lesson1_id

INSERT INTO temp_lessons_ids SELECT 'lesson1', id FROM lesson1_id;

WITH lesson2_id AS (
    INSERT INTO lessons (name, difficulty, theory)
        VALUES ('Buying things', 1, 'Lesson 2')
        RETURNING id) --lesson2_id

INSERT INTO temp_lessons_ids SELECT 'lesson2', id FROM lesson2_id;

WITH lesson3_id AS (
    INSERT INTO lessons (name, difficulty, theory)
        VALUES ('Relationship', 2, 'Lesson 1')
        RETURNING id) --lesson3_id

INSERT INTO temp_lessons_ids SELECT 'lesson3', id FROM lesson3_id;

--ЗАДАНИЯ
INSERT INTO tasks (audio, question, type, variants, choice_task_answer, fill_gap_answer, lesson_id)
    SELECT NULL, 'Составьте предложение из слов. jeans John''s  blue are and is his shirt white', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'John''s jeans are blue and his shirt is white', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Составьте предложение из слов. is a coat Milena wearing gloves and', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'Milena is wearing a coat and gloves', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Составьте предложение из слов. and jacket red is my my are pink shoes', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'My jacket is red and my shoes are pink', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Составьте предложение из слов. Like I  your jumper', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'I like your jumper', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Заполните пропуски в предложении. It''s cold today. I''ll wear my je_', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'ans', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Заполните пропуски в предложении. It''s cold today. I''ll wear my jeans ,and my c_ too.', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'oat', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Заполните пропуски в предложении. Lucy is going to have an interview. She''s wearing white s_', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'hirt', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Заполните пропуски в предложении. Lucy is going to have an interview. She''s wearing white shirt and black tr_.', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'ousers', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Заполните пропуски в предложении. Sarah''s d_ is old', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'ress', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Заполните пропуски в предложении. Sarah''s dress is old but her s_ are new.', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'hoes', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Заполните пропуски в предложении. It''s winter. Today I am wearing my ja_', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'cket', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Заполните пропуски в предложении. It''s winter. Today I am wearing my jacket and g_', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'loves', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Соедините фразу с её переводом. 30 pounds.', 'choice_task', CAST ('{"Мне нравится это платье. Сколько оно стоит?", "Это слишком дорого", "30 фунтов.", "Короткое зеленое."}' AS TEXT[]), CAST ('{3}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Соедините фразу с её переводом. Which one?', 'choice_task', CAST ('{"Могу я вам помочь?", "A сколько то стоит?", "Которое?/Какое?", "Могу я примерить его?"}' AS TEXT[]), CAST ('{2}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Соедините фразу с её переводом. The short green one.', 'choice_task', CAST ('{"Это слишком дорого.", "30 фунтов.", "Короткое зеленое.", "Могу я вам помочь?"}' AS TEXT[]), CAST ('{2}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Соедините фразу с её переводом. I like this dress. How much is it?', 'choice_task', CAST ('{"Это слишком дорого.", "У вас есть размер побольше/поменьше?", "Короткое зеленое.", "Мне нравится это платье. Сколько оно стоит?"}' AS TEXT[]), CAST ('{3}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Соедините фразу с её переводом. Can I help you?', 'choice_task', CAST ('{"Это слишком дорого.", "У вас есть размер побольше/поменьше?", "Короткое зеленое.", "Мне нравится это платье. Сколько оно стоит?"}' AS TEXT[]), CAST ('{3}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Составить фразы из слов. much it is how ?', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'How much is it?', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Составить фразы из слов. pounds thirty', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'Thirty pounds', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Составить фразы из слов. help you can I ?', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'Can I help you?', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Составить фразы из слов. is that how one much ?', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'How much is that one?', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Составить фразы из слов. one which ?', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'Which one?', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Составить фразы из слов. green the one short', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'The short green one.', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Вставить пропущенные слова из списка. _____ I help you?', 'choice_task', CAST ('{"One", "Much", "Can", "Expensive"}' AS TEXT[]), CAST ('{2}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Вставить пропущенные слова из списка. I like this dress. How _____ is it?', 'choice_task', CAST ('{"Pounds", "Much", "Can", "One"}' AS TEXT[]), CAST ('{1}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Вставить пропущенные слова из списка. Thirty _____.', 'choice_task', CAST ('{"Pounds", "Short", "Which", "One"}' AS TEXT[]), CAST ('{0}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Вставить пропущенные слова из списка. That’s too _____.', 'choice_task', CAST ('{"Expensive", "Can", "Much", "One"}' AS TEXT[]), CAST ('{0}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Вставить пропущенные слова из списка. How much is that _____?', 'choice_task', CAST ('{"Expensive", "Can", "Much", "One"}' AS TEXT[]), CAST ('{3}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Вставить пропущенные слова из списка. ______ one?', 'choice_task', CAST ('{"Which", "Can", "Much", "Short"}' AS TEXT[]), CAST ('{0}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Вставить пропущенные слова из списка. The _____ green one.', 'choice_task', CAST ('{"Which", "Can", "Much", "Short"}' AS TEXT[]), CAST ('{3}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson1'
    UNION ALL
    SELECT NULL, 'Соедините названия продуктов и их категорию. apple', 'choice_task', CAST ('{"fruits", "vegetables", "dairy products", "meat products"}' AS TEXT[]), CAST ('{0}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Соедините названия продуктов и их категорию. banana', 'choice_task', CAST ('{"fruits", "vegetables", "dairy products", "meat products"}' AS TEXT[]), CAST ('{0}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Соедините названия продуктов и их категорию. cucumber', 'choice_task', CAST ('{"sweets", "vegetables", "dairy products", "meat products"}' AS TEXT[]), CAST ('{1}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Соедините названия продуктов и их категорию. chicken', 'choice_task', CAST ('{"sweets", "vegetables", "dairy products", "meat products"}' AS TEXT[]), CAST ('{3}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Соедините названия продуктов и их категорию. milk', 'choice_task', CAST ('{"sweets", "vegetables", "dairy products", "meat products"}' AS TEXT[]), CAST ('{3}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Соедините названия продуктов и их категорию. cheese', 'choice_task', CAST ('{"sweets", "vegetables", "dairy products", "meat products"}' AS TEXT[]), CAST ('{3}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Соедините названия продуктов и их категорию. chocolate', 'choice_task', CAST ('{"sweets", "vegetables", "dairy products", "meat products"}' AS TEXT[]), CAST ('{0}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Поставьте слова в правильном порядке. some please would like bread I', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'I would like some bread.', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Поставьте слова в правильном порядке. much it how is', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'How much is it?', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Поставьте слова в правильном порядке. you here are', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'Here you are', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Поставьте слова в правильном порядке. help I you can', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'Can I help you?', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Поставьте слова в правильном порядке. two want cucumbers I to kilos also buy', 'fill_gap', CAST ('{}' AS TEXT[]), CAST ('{}' AS INTEGER[]), 'I also want to buy two kilos of cucumbers.', t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson2'
    UNION ALL
    SELECT NULL, 'Match the vocabulary with explanation. Date', 'choice_task', CAST ('{"A planned romantic meeting", "Formally agree to get married", "Become friends with somebody", "End a romantic relationship with somebody"}' AS TEXT[]), CAST ('{0}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson3'
    UNION ALL
    SELECT NULL, 'Match the vocabulary with explanation. Brake up with', 'choice_task', CAST ('{"A planned romantic meeting", "Formally agree to get married", "Become friends with somebody", "End a romantic relationship with somebody"}' AS TEXT[]), CAST ('{3}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson3'
    UNION ALL
    SELECT NULL, 'Match the vocabulary with explanation. Get to know', 'choice_task', CAST ('{"A planned romantic meeting", "Formally agree to get married", "Become friends with somebody", "End a romantic relationship with somebody"}' AS TEXT[]), CAST ('{2}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson3'
    UNION ALL
    SELECT NULL, 'Match the vocabulary with explanation. Get engaged', 'choice_task', CAST ('{"A planned romantic meeting", "Formally agree to get married", "Become friends with somebody", "End a romantic relationship with somebody"}' AS TEXT[]), CAST ('{1}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson3'
    UNION ALL
    SELECT NULL, 'Fill in the correct word. Did you really ______ up with Alex? I''m so sorry to hear that!', 'choice_task', CAST ('{"Date", "Brake", "Go", "Know"}' AS TEXT[]), CAST ('{1}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson3'
    UNION ALL
    SELECT NULL, 'Fill in the correct word. I''m going on a _______ with Jake today. I think we will go to this French restaurant in the city center.', 'choice_task', CAST ('{"Date", "Brake", "Go", "Know"}' AS TEXT[]), CAST ('{0}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson3'
    UNION ALL
    SELECT NULL, 'Fill in the correct word. Why don’t you ______ out with Jake? You would be a nice couple.', 'choice_task', CAST ('{"Date", "Brake", "Go", "Know"}' AS TEXT[]), CAST ('{2}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson3'
    UNION ALL
    SELECT NULL, 'Don''t be so shy, ask him about his hobbies! This simple question will help you to get to _______ each out.', 'choice_task', CAST ('{"Date", "Brake", "Go", "Know"}' AS TEXT[]), CAST ('{3}' AS INTEGER[]), NULL, t.lesson_id
        FROM temp_lessons_ids AS t WHERE lesson_name = 'lesson3';

DROP TABLE temp_lessons_ids;
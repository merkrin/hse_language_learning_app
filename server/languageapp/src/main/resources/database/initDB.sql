CREATE TABLE IF NOT EXISTS shop (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(200) NOT NULL,
    description VARCHAR NOT NULL,
    available_now BOOLEAN
);

CREATE TABLE IF NOT EXISTS shop_transactions (
    itemId BIGINT NOT NULL,
    userId BIGINT NOT NULL,
    cost INTEGER NOT NULL,
    time TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
    id BIGSERIAL PRIMARY KEY,
    email VARCHAR(320) NOT NULL UNIQUE,
    nickname VARCHAR(30) NOT NULL,
    country VARCHAR(10),
    age INTEGER,
    mother_lang VARCHAR(10),
    balance BIGINT NOT NULL,
    language_level INTEGER,
    last_login TIMESTAMP NOT NULL
);

ALTER TABLE users ALTER COLUMN language_level DROP NOT NULL;
ALTER TABLE users ADD UNIQUE (email);

CREATE TABLE IF NOT EXISTS feedback (
    user_id BIGINT NOT NULL,
    grade INTEGER NOT NULL,
    comment VARCHAR,
    time TIMESTAMP NOT NULL
);

CREATE TABLE IF NOT EXISTS lesson_progress (
    user_id BIGINT NOT NULL,
    lesson_id BIGINT NOT NULL,
    mistakes INTEGER,
    retries INTEGER,
    status VARCHAR(20) NOT NULL,
    progress INTEGER NOT NULL
);

ALTER TABLE lesson_progress ADD UNIQUE (user_id, lesson_id);

CREATE TABLE IF NOT EXISTS curriculums (
    id BIGSERIAL PRIMARY KEY,
    user_id BIGINT UNIQUE NOT NULL
);

CREATE TABLE IF NOT EXISTS curriculum_lessons (
    curriculum_id BIGINT NOT NULL,
    lesson_id BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS lessons (
    id BIGSERIAL PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    difficulty INTEGER NOT NULL,
    theory TEXT
);

CREATE TABLE IF NOT EXISTS tasks (
    id BIGSERIAL PRIMARY KEY,
    audio VARCHAR,
    question VARCHAR NOT NULL,
    type VARCHAR NOT NULL,
    variants TEXT[],
    choice_task_answer INTEGER[],
    fill_gap_answer VARCHAR,
    lesson_id BIGINT NOT NULL
);
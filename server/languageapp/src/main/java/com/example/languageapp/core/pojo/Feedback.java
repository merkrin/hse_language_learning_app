package com.example.languageapp.core.pojo;

import com.example.languageapp.androidclient.response.FeedbackDto;
import org.joda.time.Instant;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class Feedback implements Comparable<Feedback> {
    private final long userId;
    private final int grade;
    private final Instant time;
    private final Optional<String> review;

    public Feedback(long userId, int grade, Instant time, Optional<String> review) {
        this.grade = grade;
        this.time = time;
        this.userId = userId;
        this.review = review;
    }

    public static Feedback fromFeedbackDto(FeedbackDto feedbackDto, long userId) {
        return new Feedback(userId, feedbackDto.getGrade(), Instant.now(), feedbackDto.getReview());
    }

    public long getUserId() {
        return userId;
    }

    public int getGrade() {
        return grade;
    }

    public Instant getTime() {
        return time;
    }

    public Optional<String> getReview() {
        return review;
    }

    @Override
    public int compareTo(Feedback anotherFeedback) {
        return this.time.compareTo(anotherFeedback.time);
    }

    public static class Mapper implements RowMapper<Feedback> {
        @Override
        public Feedback mapRow(ResultSet rs, int rowNum) throws SQLException {
            int grade = rs.getInt("grade");
            long userId = rs.getLong("user_id");
            Instant time = new Instant(rs.getTimestamp("time").getTime());
            Optional<String> review = Optional.ofNullable(rs.getString("comment"));
            return new Feedback(userId, grade, time, review);
        }
    }
}

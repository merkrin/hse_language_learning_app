package com.example.languageapp.core.authorization;

public class AuthorizedUser {
    private final long id;
    private final String email;
    private final Role role;

    public AuthorizedUser(long id, String email, Role role) {
        this.id = id;
        this.email = email;
        this.role = role;
    }

    public long getId() {
        return id;
    }

    public Role getRole() {
        return role;
    }

    public String getEmail() {
        return email;
    }
}

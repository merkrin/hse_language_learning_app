package com.example.languageapp.admin.response;

import com.example.languageapp.core.pojo.Feedback;

import java.util.Map;

public class FeedbackForAdminDto {
    private final Map<Integer, Long> gradesCnt;

    public FeedbackForAdminDto(Map<Integer, Long> gradesCnt) {
        this.gradesCnt = gradesCnt;
    }

    public FeedbackForAdminDto(Feedback feedback) {
        this.gradesCnt = Map.of(feedback.getGrade(), 1L);
    }

    public Map<Integer, Long> getGradesCnt() {
        return gradesCnt;
    }
}

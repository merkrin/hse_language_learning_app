package com.example.languageapp.core.pojo;

import com.example.languageapp.core.pojo.enums.Country;
import com.example.languageapp.core.pojo.enums.Language;
import com.example.languageapp.core.pojo.enums.LanguageLevel;

public class User {
    public Long id;
    public String nickname;
    public Integer age;
    public Country country;
    public Language motherLang;
    public LanguageLevel level;
    public Long balance;
    public Long lastLoginTs;
    public String email;

    public User() {
    }

    public User(long id, String nickname, int age, Country country, Language motherLang, LanguageLevel level,
                long balance, long lastLoginTs, String email) {
        this.id = id;
        this.nickname = nickname;
        this.age = age;
        this.country = country;
        this.motherLang = motherLang;
        this.level = level;
        this.balance = balance;
        this.lastLoginTs = lastLoginTs;
        this.email = email;
    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Language getMotherLang() {
        return motherLang;
    }

    public void setMotherLang(Language motherLang) {
        this.motherLang = motherLang;
    }

    public LanguageLevel getLevel() {
        return level;
    }

    public void setLevel(LanguageLevel level) {
        this.level = level;
    }

    public long getBalance() {
        return balance;
    }

    public void setBalance(Long balance) {
        this.balance = balance;
    }

    public long getLastLoginTs() {
        return lastLoginTs;
    }

    public void setLastLoginTs(Long lastLoginTs) {
        this.lastLoginTs = lastLoginTs;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}

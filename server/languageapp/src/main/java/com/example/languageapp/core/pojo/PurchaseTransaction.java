package com.example.languageapp.core.pojo;

import org.joda.time.Instant;

public class PurchaseTransaction {
    private final long id;
    private final long itemId;
    private final long userId;

    private final int cost;
    private final Instant time;

    public PurchaseTransaction(long id, long itemId, long userId, int cost, Instant time) {
        this.id = id;
        this.itemId = itemId;
        this.userId = userId;
        this.cost = cost;
        this.time = time;
    }

    public long getId() {
        return id;
    }

    public long getItemId() {
        return itemId;
    }

    public long getUserId() {
        return userId;
    }

    public int getCost() {
        return cost;
    }

    public Instant getTime() {
        return time;
    }
}

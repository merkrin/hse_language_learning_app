package com.example.languageapp.androidclient.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

public class TestResultDto {
    @Min(value = 0, message = "Test result cannot be less than 0%")
    @Max(value = 100, message = "Test result cannot be more than 100%")
    private final int percent;

    @JsonCreator
    public TestResultDto(@JsonProperty(required = true) int percent) {
        this.percent = percent;
    }

    public int getPercent() {
        return percent;
    }
}

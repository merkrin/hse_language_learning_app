package com.example.languageapp.androidclient.response;

import com.example.languageapp.core.pojo.Curriculum;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.stream.Collectors;

public class LessonsMapDto {
    private final List<LessonWithProgressDto> lessons;

    @JsonCreator
    public LessonsMapDto(@JsonProperty List<LessonWithProgressDto> lessons) {
        this.lessons = lessons;
    }

    public static LessonsMapDto fromCurriculum(Curriculum curriculum) {
        return new LessonsMapDto(curriculum.getLessons().stream().map(LessonWithProgressDto::fromLessonWithProgress)
                .collect(Collectors.toList()));
    }

    public List<LessonWithProgressDto> getLessons() {
        return lessons;
    }
}

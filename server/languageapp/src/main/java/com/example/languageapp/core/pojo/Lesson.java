package com.example.languageapp.core.pojo;

import java.util.List;
import java.util.Optional;

public class Lesson extends LessonBase {
    private final List<Task> tasks;
    private final int taskCount;

    public Lesson(long id, String name, List<Task> tasks, Optional<String> theory, int difficulty, int taskCount) {
        super(id, name, theory, difficulty);
        this.tasks = tasks;
        this.taskCount = taskCount;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public int getTaskCount() {
        return taskCount;
    }
}

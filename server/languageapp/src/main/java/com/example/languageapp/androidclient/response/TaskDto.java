package com.example.languageapp.androidclient.response;

import com.example.languageapp.core.pojo.Task;
import com.example.languageapp.core.pojo.enums.TaskType;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Optional;

public class TaskDto {
    private final Optional<String> audio;
    private final String question;
    private final TaskType type;

    private final List<String> variants;
    private final List<Integer> choiceTaskAnswers;
    private final Optional<String> fillGapAnswer;

    @JsonCreator
    public TaskDto(
            @JsonProperty Optional<String> audio,
            @JsonProperty(required = true) String question,
            @JsonProperty(required = true) String type,
            @JsonProperty List<String> variants,
            @JsonProperty List<Integer> choiceTaskAnswers,
            @JsonProperty Optional<String> fillGapAnswer) {
        this.audio = audio;
        this.question = question;
        this.type = TaskType.resolve(type);
        this.variants = variants;
        this.choiceTaskAnswers = choiceTaskAnswers;
        this.fillGapAnswer = fillGapAnswer;
    }

    public static TaskDto fromTask(Task task) {
        return new TaskDto(
                task.getAudio(),
                task.getQuestion(),
                task.getType().getValue(),
                task.getVariants(),
                task.getChoiceTaskAnswers(),
                task.getFillGapAnswer());
    }

    public Optional<String> getAudio() {
        return audio;
    }

    public String getQuestion() {
        return question;
    }

    public TaskType getType() {
        return type;
    }

    public List<String> getVariants() {
        return variants;
    }

    public List<Integer> getChoiceTaskAnswers() {
        return choiceTaskAnswers;
    }

    public Optional<String> getFillGapAnswer() {
        return fillGapAnswer;
    }
}

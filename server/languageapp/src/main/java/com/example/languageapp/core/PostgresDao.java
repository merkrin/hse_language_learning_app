package com.example.languageapp.core;

import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public interface PostgresDao {
    default <TEntry> List<TEntry> findById(long id, Class<TEntry> clazz) {
        return getJdbcTemplate().query("SELECT * FROM " + getTableName() + " WHERE " + getIdFieldName() + " = ?",
                new Object[]{id},
                new int[]{Types.BIGINT},
                new BeanPropertyRowMapper<>(clazz));
    }

    default <TEntry> List<TEntry> findAll(Class<TEntry> clazz) {
        return getJdbcTemplate().query("SELECT * FROM " + getTableName(), new BeanPropertyRowMapper<>(clazz));
    }

    default boolean update(long id, Map<String, ValueWithType> valuesToInsert) {
        List<String> columns = new ArrayList<>(valuesToInsert.keySet());
        String setExpression = columns.stream().map(x -> x + " = ?").collect(Collectors.joining(", "));

        Object[] values = Stream.concat(columns.stream().map(x -> valuesToInsert.get(x).value), Stream.of(id)).toArray();

        int[] types = Stream.concat(columns.stream().map(x -> valuesToInsert.get(x).sqlType), Stream.of(Types.BIGINT))
                .mapToInt(x -> x).toArray();

        return getJdbcTemplate().update("UPDATE " + getTableName() + " SET " + setExpression +
                " WHERE " + getIdFieldName() + " = ?", values, types) > 0;
    }

    default boolean add(Map<String, ValueWithType> valuesToInsert) {
        List<String> columns = new ArrayList<>(valuesToInsert.keySet());
        String valuesHolder = IntStream.range(0, columns.size()).mapToObj(x -> "?").collect(Collectors.joining(", "));

        Object[] values = columns.stream().map(x -> valuesToInsert.get(x).value).toArray();
        int[] types = columns.stream().map(x -> valuesToInsert.get(x).sqlType).mapToInt(x -> x).toArray();

        return getJdbcTemplate().update(
                "INSERT INTO " + getTableName() + " (" + String.join(", ", columns) + ") " +
                        "VALUES (" + valuesHolder + ")",
                values,
                types) == 1;
    }

    default Long addAndReturnId(Map<String, ValueWithType> valuesToInsert) {
        return addAndReturnId(valuesToInsert, getIdFieldName());
    }

    default long addAndReturnId(Map<String, ValueWithType> valuesToInsert, String idField) {
        List<String> columns = new ArrayList<>(valuesToInsert.keySet());
        String valuesHolder = IntStream.range(0, columns.size()).mapToObj(x -> "?").collect(Collectors.joining(", "));

        Object[] values = columns.stream().map(x -> valuesToInsert.get(x).value).toArray();
        int[] types = columns.stream().map(x -> valuesToInsert.get(x).sqlType).mapToInt(x -> x).toArray();

        String query = "INSERT INTO " + getTableName() + " (" + String.join(", ", columns) + ") " +
                "VALUES (" + valuesHolder + ")";

        PreparedStatementCreatorFactory psFactory = new PreparedStatementCreatorFactory(query, types);
        psFactory.setReturnGeneratedKeys(true);
        psFactory.setGeneratedKeysColumnNames(idField);

        PreparedStatementCreator psc = psFactory.newPreparedStatementCreator(values);
        KeyHolder keyHolder = new GeneratedKeyHolder();

        getJdbcTemplate().update(psc, keyHolder);

        if (keyHolder.getKeys().size() > 1) {
            return (Long) keyHolder.getKeys().get(idField);
        } else {
            return keyHolder.getKey().longValue();
        }
    }

    String getTableName();

    String getIdFieldName();

    JdbcTemplate getJdbcTemplate();
}

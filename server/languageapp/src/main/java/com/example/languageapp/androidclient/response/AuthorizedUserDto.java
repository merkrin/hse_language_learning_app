package com.example.languageapp.androidclient.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Email;

public class AuthorizedUserDto {
    @Email
    private final String email;

    @JsonCreator
    public AuthorizedUserDto(@JsonProperty(required = true) String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }
}

package com.example.languageapp.androidclient.response;

import com.example.languageapp.core.pojo.LessonWithProgress;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LessonWithProgressDto {
    private final LessonDto lesson;
    private final LessonProgressDto lessonProgress;

    @JsonCreator
    public LessonWithProgressDto(
            @JsonProperty LessonDto lesson,
            @JsonProperty LessonProgressDto lessonProgress) {
        this.lesson = lesson;
        this.lessonProgress = lessonProgress;
    }

    public static LessonWithProgressDto fromLessonWithProgress(LessonWithProgress lessonWithProgress) {
        return new LessonWithProgressDto(
                LessonDto.fromLesson(lessonWithProgress.getLesson()),
                LessonProgressDto.fromLessonProgress(lessonWithProgress.getLessonProgress()));
    }

    public LessonDto getLesson() {
        return lesson;
    }

    public LessonProgressDto getLessonProgress() {
        return lessonProgress;
    }
}

package com.example.languageapp.core.authorization;

public enum Role {
    ADMIN,
    USER;

    public static Role resolve(String role) {
        return Enum.valueOf(Role.class, role.trim().toUpperCase());
    }
}

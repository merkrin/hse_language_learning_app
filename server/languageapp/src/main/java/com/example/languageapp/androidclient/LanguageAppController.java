package com.example.languageapp.androidclient;

import com.example.languageapp.androidclient.response.*;
import com.example.languageapp.core.authorization.AuthorizedUser;
import com.example.languageapp.core.authorization.JwtTokenResponse;
import com.example.languageapp.core.pojo.Feedback;
import io.jsonwebtoken.JwtException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.logging.Logger;

@RestController
@RequestMapping("api")
public class LanguageAppController {
    private static final Logger logger = Logger.getLogger(LanguageAppController.class.getName());

    private final LanguageAppManager manager;

    public LanguageAppController(LanguageAppManager manager) {
        this.manager = manager;
    }

    @PostMapping(value = "/add-feedback")
    public ResponseEntity addFeedback(@RequestHeader("Authorization") String token,
                                      @Valid @RequestBody FeedbackDto feedback,
                                      BindingResult bindingResult) {
        processBindingErrors(bindingResult);
        AuthorizedUser user = checkAccess(token);

        manager.addFeedback(Feedback.fromFeedbackDto(feedback, user.getId()));
        return ResponseEntity.ok().build();
    }

    @PostMapping(value = "/authorize")
    public JwtTokenResponse authorize(@Valid @RequestBody AuthorizedUserDto userDto, BindingResult bindingResult) {
        processBindingErrors(bindingResult);

        return new JwtTokenResponse(manager.authorizeUser(userDto));
    }

    @GetMapping(value = "/get-lessons")
    public LessonsMapDto getLessonsMap(@RequestHeader("Authorization") String token) {
        AuthorizedUser user = checkAccess(token);

        return manager.getLessonsMap(user);
    }

    @GetMapping(value = "/get-test")
    public LessonDto getIntroTest(@RequestHeader("Authorization") String token) {
        checkAccess(token);

        return manager.getIntroTest();
    }

    @PostMapping(value = "/test-results")
    public LessonsMapDto proceedTestResults(
            @RequestHeader("Authorization") String token,
            @Valid @RequestBody TestResultDto testResultDto,
            BindingResult bindingResult) {
        processBindingErrors(bindingResult);
        AuthorizedUser user = checkAccess(token);
        return manager.computeLessonsMap(user, testResultDto);
    }

    private Optional<FieldError> getFieldError(List<ObjectError> errors) {
        for (ObjectError error : errors) {
            if (error instanceof FieldError) {
                return Optional.of((FieldError) error);
            }
        }
        return Optional.empty();
    }

    private AuthorizedUser checkAccess(String token) {
        AuthorizedUser user;
        try {
            user = manager.checkAccess(token);
            return user;
        } catch (JwtException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    private void processBindingErrors(BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            Optional<FieldError> fieldError = getFieldError(bindingResult.getAllErrors());
            if (fieldError.isPresent()) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        String.format("Bad value %s for field %s",
                                fieldError.get().getRejectedValue(), fieldError.get().getField()));
            } else {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                        bindingResult.getAllErrors().get(0).getDefaultMessage());
            }
        }
    }
}

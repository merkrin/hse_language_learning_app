package com.example.languageapp.core.pojo;

import java.util.List;

public class Curriculum {
    private final long id;
    private final List<LessonWithProgress> lessons;
    private final long userId;

    public Curriculum(long id, List<LessonWithProgress> lessons, long userId) {
        this.id = id;
        this.lessons = lessons;
        this.userId = userId;
    }

    public long getId() {
        return id;
    }

    public List<LessonWithProgress> getLessons() {
        return lessons;
    }

    public long getUserId() {
        return userId;
    }
}

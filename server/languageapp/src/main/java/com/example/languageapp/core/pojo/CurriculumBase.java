package com.example.languageapp.core.pojo;

public class CurriculumBase {
    private long userId;
    private long id;

    public CurriculumBase(long userId, long id) {
        this.userId = userId;
        this.id = id;
    }

    public CurriculumBase() {
    }

    public long getUserId() {
        return userId;
    }

    public long getId() {
        return id;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public void setId(long id) {
        this.id = id;
    }
}

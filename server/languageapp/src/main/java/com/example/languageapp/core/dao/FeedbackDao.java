package com.example.languageapp.core.dao;

import com.example.languageapp.core.PostgresDao;
import com.example.languageapp.core.ValueWithType;
import com.example.languageapp.core.pojo.Feedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
@Transactional
public class FeedbackDao extends JdbcDaoSupport implements PostgresDao {
    @Autowired
    public FeedbackDao(DataSource dataSource) {
        this.setDataSource(dataSource);
    }

    public Optional<Feedback> findById(long userId) {
        return findById(userId, Feedback.class).stream().max(Feedback::compareTo);
    }

    public List<Feedback> findAll() {
        return findAll(Feedback.class);
    }

    @Override
    public boolean update(long id, Map<String, ValueWithType> valuesToInsert) {
        throw new UnsupportedOperationException("Updates not available for feedback table");
    }

    public boolean add(Feedback feedback) {
        HashMap<String, ValueWithType> paramsMap = new HashMap<>(Map.of(
                "user_id", new ValueWithType(feedback.getUserId(), Types.BIGINT),
                "grade", new ValueWithType(feedback.getGrade(), Types.INTEGER),
                "time", new ValueWithType(
                        new Timestamp(feedback.getTime().getMillis()), Types.TIMESTAMP)));

        feedback.getReview().filter(x -> !x.isBlank()).ifPresent(x -> paramsMap.put("comment", new ValueWithType(
                x, Types.VARCHAR)));

        return add(paramsMap);
    }

    @Override
    public String getTableName() {
        return "feedback";
    }

    @Override
    public String getIdFieldName() {
        return "user_id";
    }
}

package com.example.languageapp.core.pojo;

import com.example.languageapp.core.pojo.enums.TaskType;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Task {
    private long id;
    private long lessonId;
    private final Optional<String> audio;
    private final String question;
    private final TaskType type;
    private final List<String> variants;
    private final List<Integer> choiceTaskAnswers;
    private final Optional<String> fillGapAnswer;

    public Task(long id, long lessonId, Optional<String> audio, String question, String type,
                List<String> variants, List<Integer> choiceTaskAnswers, Optional<String> fillGapAnswer) {
        this.id = id;
        this.lessonId = lessonId;
        this.audio = audio;
        this.question = question;
        this.type = TaskType.resolve(type);
        this.variants = variants;
        this.choiceTaskAnswers = choiceTaskAnswers;
        this.fillGapAnswer = fillGapAnswer;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getLessonId() {
        return lessonId;
    }

    public void setLessonId(long lessonId) {
        this.lessonId = lessonId;
    }

    public Optional<String> getAudio() {
        return audio;
    }

    public String getQuestion() {
        return question;
    }

    public TaskType getType() {
        return type;
    }

    public List<String> getVariants() {
        return variants;
    }

    public List<Integer> getChoiceTaskAnswers() {
        return choiceTaskAnswers;
    }

    public Optional<String> getFillGapAnswer() {
        return fillGapAnswer;
    }

    public static class TaskRowMapper implements RowMapper<Task> {

        @Override
        public Task mapRow(ResultSet rs, int rowNum) throws SQLException {
            long id = rs.getLong("id");
            long lessonId = rs.getLong("lesson_id");
            Optional<String> audio = Optional.ofNullable(rs.getString("audio"));
            String question = rs.getString("question");
            String taskType = rs.getString("type");
            List<String> variants = Arrays.asList((String[]) (rs.getArray("variants").getArray()));
            List<Integer> choiceTaskAnswer = Arrays.asList((Integer[]) (rs.getArray("choice_task_answer").getArray()));
            Optional<String> fillGapAnswer = Optional.ofNullable(rs.getString("fill_gap_answer"));

            return new Task(id, lessonId, audio, question, taskType, variants, choiceTaskAnswer, fillGapAnswer);
        }
    }
}

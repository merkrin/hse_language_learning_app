package com.example.languageapp.core.pojo;

import java.util.Optional;

public class LessonBase {
    private long id;
    private String name;
    private Optional<String> theory;
    private int difficulty;

    public LessonBase() {
    }

    public LessonBase(long id, String name, Optional<String> theory, int difficulty) {
        this.id = id;
        this.name = name;
        this.theory = theory;
        this.difficulty = difficulty;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Optional<String> getTheory() {
        return theory;
    }

    public void setTheory(Optional<String> theory) {
        this.theory = theory;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }
}

package com.example.languageapp.core.pojo;

public class PurchaseItem {
    private final long id;
    private final int cost;
    private final String description;
    private final String name;
    private final boolean availableNow;

    public PurchaseItem(long id, int cost, String description, String name, boolean availableNow) {
        this.id = id;
        this.cost = cost;
        this.description = description;
        this.name = name;
        this.availableNow = availableNow;
    }

    public long getId() {
        return id;
    }

    public int getCost() {
        return cost;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public boolean isAvailableNow() {
        return availableNow;
    }
}

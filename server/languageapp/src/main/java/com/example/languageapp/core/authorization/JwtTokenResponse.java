package com.example.languageapp.core.authorization;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class JwtTokenResponse {
    private final String token;

    @JsonCreator
    public JwtTokenResponse(@JsonProperty(required = true) String token) {
        this.token = token;
    }

    public String getToken() {
        return token;
    }
}

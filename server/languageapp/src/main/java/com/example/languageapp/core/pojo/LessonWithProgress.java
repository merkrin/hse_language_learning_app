package com.example.languageapp.core.pojo;

public class LessonWithProgress {
    private Lesson lesson;
    private LessonProgress lessonProgress;

    public LessonWithProgress() {
    }

    public LessonWithProgress(Lesson lesson, LessonProgress lessonProgress) {
        this.lesson = lesson;
        this.lessonProgress = lessonProgress;
    }

    public static LessonWithProgress withDefaultProgress(Lesson lesson, long userId) {
        return new LessonWithProgress(lesson, LessonProgress.getDefaultProgress(userId, lesson.getId()));
    }

    public Lesson getLesson() {
        return lesson;
    }

    public LessonProgress getLessonProgress() {
        return lessonProgress;
    }
}

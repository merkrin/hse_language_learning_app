package com.example.languageapp.core.dao;

import com.example.languageapp.core.PostgresDao;
import com.example.languageapp.core.ValueWithType;
import com.example.languageapp.core.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.Instant;
import java.util.Map;
import java.util.Optional;

@Repository
@Transactional
public class UsersDao extends JdbcDaoSupport implements PostgresDao {

    private static final String QUERY_GET_USER_BY_EMAIL = "SELECT * FROM users WHERE email = ?";

    @Autowired
    public UsersDao(DataSource dataSource) {
        this.setDataSource(dataSource);
    }

    public Optional<User> findById(long userId) {
        return findById(userId, User.class).stream().findFirst();
    }

    public Long addNewUser(String email) {
        Optional<User> existing = getExistingUser(email);

        if (existing.isPresent()) {
            return existing.get().getId();
        }

        Map<String, ValueWithType> params = Map.of(
                "email", new ValueWithType(email, Types.VARCHAR),
                "nickname", new ValueWithType(email, Types.VARCHAR),
                "balance", new ValueWithType(0, Types.BIGINT),
                "last_login", new ValueWithType(Timestamp.from(Instant.now()), Types.TIMESTAMP));
        return addAndReturnId(params);
    }

    public Optional<User> getExistingUser(String email) {
        return getJdbcTemplate().query(QUERY_GET_USER_BY_EMAIL,
                new String[]{email},
                new int[]{Types.VARCHAR},
                new BeanPropertyRowMapper<>(User.class)).stream().findFirst();
    }

    @Override
    public String getTableName() {
        return "users";
    }

    @Override
    public String getIdFieldName() {
        return "id";
    }
}

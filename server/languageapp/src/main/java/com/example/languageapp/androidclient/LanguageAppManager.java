package com.example.languageapp.androidclient;

import com.example.languageapp.androidclient.response.AuthorizedUserDto;
import com.example.languageapp.androidclient.response.LessonDto;
import com.example.languageapp.androidclient.response.LessonsMapDto;
import com.example.languageapp.androidclient.response.TestResultDto;
import com.example.languageapp.core.authorization.AuthorizedUser;
import com.example.languageapp.core.authorization.JwtUtils;
import com.example.languageapp.core.authorization.Role;
import com.example.languageapp.core.dao.FeedbackDao;
import com.example.languageapp.core.dao.LessonsDao;
import com.example.languageapp.core.dao.UsersDao;
import com.example.languageapp.core.pojo.Curriculum;
import com.example.languageapp.core.pojo.Feedback;
import com.example.languageapp.core.pojo.Lesson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;

import java.util.logging.Logger;

@Component
public class LanguageAppManager {
    private final Logger logger = Logger.getLogger(LanguageAppManager.class.getName());

    private final FeedbackDao feedbackDao;
    private final UsersDao usersDao;
    private final LessonsDao lessonsDao;
    private final JwtUtils jwtUtils;

    @Autowired
    public LanguageAppManager(FeedbackDao feedbackDao, UsersDao usersDao, LessonsDao lessonsDao, JwtUtils jwtUtils) {
        this.feedbackDao = feedbackDao;
        this.usersDao = usersDao;
        this.lessonsDao = lessonsDao;
        this.jwtUtils = jwtUtils;
    }

    public void addFeedback(Feedback feedback) {
        feedbackDao.add(feedback);
        logger.info("Feedback added");
    }

    public String authorizeUser(AuthorizedUserDto userDto) {
        Long id = usersDao.addNewUser(userDto.getEmail());
        return jwtUtils.generateToken(new AuthorizedUser(id, userDto.getEmail(), Role.USER));
    }

    public AuthorizedUser checkAccess(String token) {
        AuthorizedUser user = jwtUtils.parseToken(token);

        if (usersDao.findById(user.getId()).isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found");
        }

        return user;
    }

    public LessonsMapDto getLessonsMap(AuthorizedUser user) {
        Curriculum curriculum = lessonsDao.getUserCurriculum(user.getId());

        return LessonsMapDto.fromCurriculum(curriculum);
    }

    public LessonsMapDto computeLessonsMap(AuthorizedUser user, TestResultDto testResultDto) {
        Curriculum curriculum = lessonsDao.computeUserCurriculum(user.getId(), testResultDto.getPercent());

        return LessonsMapDto.fromCurriculum(curriculum);
    }

    public LessonDto getIntroTest() {
        Lesson introTest = lessonsDao.getIntroTest();
        return LessonDto.fromLesson(introTest);
    }
}

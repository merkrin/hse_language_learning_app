package com.example.languageapp.admin;

import com.example.languageapp.admin.response.FeedbackForAdminDto;
import com.example.languageapp.core.dao.FeedbackDao;
import com.example.languageapp.core.pojo.Feedback;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("internal-api")
public class AdminController {
    private final FeedbackDao feedbackDao;

    public AdminController(FeedbackDao feedbackDao) {
        this.feedbackDao = feedbackDao;
    }

    @PostMapping("/get-user-feedback")
    public FeedbackForAdminDto getFeedback(@RequestParam long userId) {
        Optional<Feedback> userFeedback = feedbackDao.findById(userId);
        if (userFeedback.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Entity not found");
        }
        return new FeedbackForAdminDto(userFeedback.get());
    }
}

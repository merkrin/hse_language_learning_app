package com.example.languageapp.core;

public class ValueWithType {
    public Object value;
    public int sqlType;

    public ValueWithType(Object value, int sqlType) {
        this.value = value;
        this.sqlType = sqlType;
    }
}

package com.example.languageapp.core.pojo.enums;

import java.util.Map;

public enum TaskType {
    FILL_GAP("fill_gap"),
    CHOICE_TASK("choice_task"),
    ;

    private static final Map<String, TaskType> valueToType = Map.of(
            FILL_GAP.value, FILL_GAP,
            CHOICE_TASK.value, CHOICE_TASK);

    private final String value;

    TaskType(String value) {
        this.value = value;
    }

    public static TaskType resolve(String value) {
        if (!valueToType.containsKey(value.toLowerCase())) {
            throw new IllegalArgumentException("Unknown enum value");
        }

        return valueToType.get(value);
    }

    public String getValue() {
        return value;
    }
}

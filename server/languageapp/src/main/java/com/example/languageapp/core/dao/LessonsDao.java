package com.example.languageapp.core.dao;

import com.example.languageapp.core.PostgresDao;
import com.example.languageapp.core.ValueWithType;
import com.example.languageapp.core.pojo.*;
import com.example.languageapp.core.pojo.enums.Difficulty;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import javax.sql.DataSource;
import java.sql.Types;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Repository
@Transactional
public class LessonsDao extends JdbcDaoSupport implements PostgresDao {
    private static final String QUERY_FOR_LESSON_IDS = "SELECT lesson_id " +
            "FROM curriculum_lessons " +
            "WHERE curriculum_id = ?";

    private static final String QUERY_FOR_LESSONS = "SELECT * FROM lessons WHERE id IN (%s)";

    private static final String QUERY_FOR_TASKS = "SELECT * FROM tasks WHERE lesson_id IN (%s)";

    private static final String QUERY_FOR_LESSONS_BY_DIFFICULTY = "SELECT * FROM lessons WHERE difficulty >= ?";

    private static final String QUERY_FOR_INTRO_TEST_ID = "SELECT id FROM lessons WHERE difficulty = 0";

    private static final String QUERY_FOR_PROGRESS = "SELECT * FROM lesson_progress WHERE user_id = ?";

    private static final String QUERY_TO_INSERT_LESSON_WITH_CURRICULUM = "INSERT INTO curriculum_lessons VALUES %s";

    @Autowired
    public LessonsDao(DataSource dataSource) {
        this.setDataSource(dataSource);
    }


    public Curriculum getUserCurriculum(long userId) {
        Optional<Long> curriculumId = findById(userId, CurriculumBase.class)
                .stream().findFirst().map(CurriculumBase::getId);

        if (curriculumId.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "User curriculum not found");
        }

        List<Long> lessonIds = getLessonIds(curriculumId.get());
        if (lessonIds.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Lessons for curriculum not found");
        }
        List<Lesson> lessons = getLessons(lessonIds);

        List<LessonProgress> progress = getProgress(userId);

        return new Curriculum(
                curriculumId.get(),
                mapLessonsToProgress(lessons, progress, userId),
                userId);
    }

    public Lesson getIntroTest() {
        Optional<Long> testId = Optional.ofNullable(getJdbcTemplate().queryForObject(
                QUERY_FOR_INTRO_TEST_ID,
                Long.class));

        if (testId.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No intro test found");
        }

        return getLessons(List.of(testId.get())).get(0);
    }

    public Curriculum computeUserCurriculum(long userId, int percent) {
        Difficulty difficulty = computeDifficulty(percent);

        List<Lesson> lessonsForCurriculum = getLessonsByDifficulty(difficulty)
                .stream().sorted(Comparator.comparingInt(LessonBase::getDifficulty)).collect(Collectors.toList());

        long curriculumId = insertCurriculum(userId, lessonsForCurriculum.stream().map(LessonBase::getId).collect(Collectors.toList()));

        return new Curriculum(
                curriculumId,
                lessonsForCurriculum
                        .stream()
                        .map(x -> LessonWithProgress.withDefaultProgress(x, userId))
                        .collect(Collectors.toList()),
                userId);
    }

    private long insertCurriculum(long userId, List<Long> lessons) {
        long curriculumId = addAndReturnId(Map.of("user_id", new ValueWithType(userId, Types.BIGINT)), "id");

        String valuesHolder = lessons.stream().map(x -> "(?, ?)").collect(Collectors.joining(", "));

        List<Long> valuesLst = new ArrayList<>();
        lessons.forEach(x -> {
            valuesLst.add(curriculumId);
            valuesLst.add(x);
        });

        Object[] values = valuesLst.toArray();

        int[] types = Arrays.stream(values).map(x -> Types.BIGINT).mapToInt(x -> x).toArray();

        getJdbcTemplate().update(
                String.format(QUERY_TO_INSERT_LESSON_WITH_CURRICULUM, valuesHolder),
                values,
                types);
        return curriculumId;
    }

    private List<Lesson> getLessonsByDifficulty(Difficulty difficulty) {
        List<LessonBase> lessonsBaseInfo = getJdbcTemplate().query(
                QUERY_FOR_LESSONS_BY_DIFFICULTY,
                ArrayUtils.toObject(new int[]{difficulty.getLevel()}),
                new int[]{Types.INTEGER},
                new BeanPropertyRowMapper<>(LessonBase.class));

        List<Long> lessonsIds = lessonsBaseInfo.stream().map(LessonBase::getId).collect(Collectors.toList());
        String valueHolderForIds = IntStream.range(0, lessonsIds.size()).mapToObj(x -> "?").collect(Collectors.joining(", "));
        Object[] idValues = lessonsIds.toArray();
        int[] idType = lessonsIds.stream().map(x -> Types.BIGINT).mapToInt(x -> x).toArray();

        List<Task> tasks = getJdbcTemplate().query(
                String.format(QUERY_FOR_TASKS, valueHolderForIds),
                idValues,
                idType,
                new Task.TaskRowMapper());

        return getLessons(tasks, lessonsBaseInfo);
    }

    private List<Lesson> getLessons(List<Task> tasks, List<LessonBase> lessonsBaseInfo) {
        Map<Long, List<Task>> groupingByLesson = tasks.stream()
                .collect(Collectors.groupingBy(Task::getLessonId));

        return lessonsBaseInfo.stream().map(x -> {
            List<Task> tasksInfo = groupingByLesson.get(x.getId());

            return new Lesson(x.getId(), x.getName(), tasksInfo,
                    x.getTheory(), x.getDifficulty(), tasksInfo.size());
        }).collect(Collectors.toList());
    }

    private List<Long> getLessonIds(long curriculumId) {
        return getJdbcTemplate().queryForList(QUERY_FOR_LESSON_IDS, new Object[]{curriculumId}, new int[]{Types.BIGINT},
                Long.class);
    }

    private List<LessonProgress> getProgress(long userId) {
        return getJdbcTemplate().query(QUERY_FOR_PROGRESS, new Object[]{userId}, new int[]{Types.BIGINT},
                new BeanPropertyRowMapper<>(LessonProgress.class));
    }

    private List<LessonWithProgress> mapLessonsToProgress(List<Lesson> lessons, List<LessonProgress> userProgress, long userId) {
        Map<Long, LessonProgress> userProgressByLesson = userProgress.stream()
                .collect(Collectors.toMap(LessonProgress::getLessonId, x -> x));
        return lessons.stream().map(x -> {
            Optional<LessonProgress> progress = Optional.ofNullable(userProgressByLesson.get(x.getId()));
            return progress.map(lessonProgress -> new LessonWithProgress(x, lessonProgress)).orElseGet(() ->
                    new LessonWithProgress(x, LessonProgress.getDefaultProgress(userId, x.getId())));
        }).collect(Collectors.toList());
    }

    private List<Lesson> getLessons(List<Long> lessonsIds) {
        String valueHolder = IntStream.range(0, lessonsIds.size()).mapToObj(x -> "?").collect(Collectors.joining(", "));
        Object[] values = lessonsIds.toArray();
        int[] types = lessonsIds.stream().map(x -> Types.BIGINT).mapToInt(x -> x).toArray();

        List<LessonBase> lessonsBaseInfo = getJdbcTemplate().query(
                String.format(QUERY_FOR_LESSONS, valueHolder),
                values,
                types,
                new BeanPropertyRowMapper<>(LessonBase.class));

        List<Task> tasks = getJdbcTemplate().query(
                String.format(QUERY_FOR_TASKS, valueHolder),
                values,
                types,
                new Task.TaskRowMapper());

        return getLessons(tasks, lessonsBaseInfo);
    }

    private Difficulty computeDifficulty(int percent) {
        Difficulty difficulty;

        if (percent < 20) {
            difficulty = Difficulty.BEGINNER;
        } else if (percent < 40) {
            difficulty = Difficulty.ELEMENTARY;
        } else if (percent < 60) {
            difficulty = Difficulty.INTERMEDIATE;
        } else if (percent < 90) {
            difficulty = Difficulty.UPPER_INTERMEDIATE;
        } else {
            difficulty = Difficulty.ADVANCED;
        }
        return difficulty;
    }

    @Override
    public String getTableName() {
        return "curriculums";
    }

    @Override
    public String getIdFieldName() {
        return "user_id";
    }

}

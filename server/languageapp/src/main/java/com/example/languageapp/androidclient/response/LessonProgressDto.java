package com.example.languageapp.androidclient.response;

import com.example.languageapp.core.pojo.LessonProgress;
import com.example.languageapp.core.pojo.enums.LessonStatus;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class LessonProgressDto {
    private final int mistakes;
    private final int progress;
    private final int retries;
    private final LessonStatus status;

    @JsonCreator
    public LessonProgressDto(
            @JsonProperty(defaultValue = "0") int mistakes,
            @JsonProperty(required = true) int progress,
            @JsonProperty(defaultValue = "1") int retries,
            @JsonProperty(defaultValue = "COMPLETED") String status) {
        this.mistakes = mistakes;
        this.progress = progress;
        this.retries = retries;
        this.status = LessonStatus.resolve(status);
    }

    public static LessonProgressDto fromLessonProgress(LessonProgress lessonProgress) {
        return new LessonProgressDto(
                lessonProgress.getMistakes(),
                lessonProgress.getProgress(),
                lessonProgress.getRetries(),
                lessonProgress.getStatus().getValue());
    }

    public int getMistakes() {
        return mistakes;
    }

    public int getProgress() {
        return progress;
    }

    public int getRetries() {
        return retries;
    }

    public LessonStatus getStatus() {
        return status;
    }
}

package com.example.languageapp.androidclient.response;

import com.example.languageapp.core.pojo.Lesson;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class LessonDto {
    private final String name;
    private final Optional<String> theory;
    private final int difficulty;
    private final List<TaskDto> tasks;
    private final int taskCount;

    @JsonCreator
    public LessonDto(
            @JsonProperty(required = true) String name,
            @JsonProperty Optional<String> theory,
            @JsonProperty(required = true) int difficulty,
            @JsonProperty List<TaskDto> tasks,
            @JsonProperty int taskCount) {
        this.name = name;
        this.theory = theory;
        this.difficulty = difficulty;
        this.tasks = tasks;
        this.taskCount = taskCount;
    }

    public static LessonDto fromLesson(Lesson lesson) {
        return new LessonDto(
                lesson.getName(),
                lesson.getTheory(),
                lesson.getDifficulty(),
                lesson.getTasks().stream().map(TaskDto::fromTask).collect(Collectors.toList()),
                lesson.getTaskCount());
    }

    public String getName() {
        return name;
    }

    public Optional<String> getTheory() {
        return theory;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public List<TaskDto> getTasks() {
        return tasks;
    }

    public int getTaskCount() {
        return taskCount;
    }
}

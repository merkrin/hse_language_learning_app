package com.example.languageapp.core.pojo;

import com.example.languageapp.core.pojo.enums.LessonStatus;

public class LessonProgress {
    private long userId;
    private long lessonId;
    private int mistakes;
    private int progress;
    private int retries;
    private LessonStatus status;

    public LessonProgress() {
    }

    public LessonProgress(long userId, long lessonId, int mistakes, int progress, int retries, String status) {
        this.userId = userId;
        this.lessonId = lessonId;
        this.mistakes = mistakes;
        this.progress = progress;
        this.retries = retries;
        this.status = LessonStatus.resolve(status);
    }

    public static LessonProgress getDefaultProgress(long userId, long lessonId) {
        return new LessonProgress(userId, lessonId, 0, 0, 0, LessonStatus.NOT_STARTED.getValue());
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getLessonId() {
        return lessonId;
    }

    public void setLessonId(long lessonId) {
        this.lessonId = lessonId;
    }

    public int getMistakes() {
        return mistakes;
    }

    public void setMistakes(int mistakes) {
        this.mistakes = mistakes;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getRetries() {
        return retries;
    }

    public void setRetries(int retries) {
        this.retries = retries;
    }

    public LessonStatus getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = LessonStatus.resolve(status);
    }
}

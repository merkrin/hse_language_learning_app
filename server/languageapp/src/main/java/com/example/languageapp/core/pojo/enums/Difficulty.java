package com.example.languageapp.core.pojo.enums;

import java.util.Map;

public enum Difficulty {
    INTRO(0),
    BEGINNER(1),
    ELEMENTARY(2),
    INTERMEDIATE(3),
    UPPER_INTERMEDIATE(4),
    ADVANCED(5),
    ;

    private static final Map<Integer, Difficulty> valueToType = Map.of(
            INTRO.level, INTRO,
            BEGINNER.level, BEGINNER,
            ELEMENTARY.level, ELEMENTARY,
            INTERMEDIATE.level, INTERMEDIATE,
            UPPER_INTERMEDIATE.level, UPPER_INTERMEDIATE,
            ADVANCED.level, ADVANCED);


    private final int level;

    Difficulty(int level) {
        this.level = level;
    }

    public static Difficulty resolve(int value) {
        if (!valueToType.containsKey(value)) {
            throw new IllegalArgumentException("Unknown enum value");
        }

        return valueToType.get(value);
    }

    public int getLevel() {
        return level;
    }
}

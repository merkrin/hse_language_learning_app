package com.example.languageapp.core.pojo.enums;

import java.util.Map;

public enum LessonStatus {
    UNAVAILABLE,
    NOT_STARTED,
    IN_PROGRESS,
    COMPLETED,
    ;
    private static final Map<String, LessonStatus> valueToType = Map.of(
            UNAVAILABLE.getValue(), UNAVAILABLE,
            NOT_STARTED.getValue(), NOT_STARTED,
            IN_PROGRESS.getValue(), IN_PROGRESS,
            COMPLETED.getValue(), COMPLETED);

    public static LessonStatus resolve(String value) {
        if (!valueToType.containsKey(value.toLowerCase())) {
            throw new IllegalArgumentException("Unknown enum value");
        }

        return valueToType.get(value);
    }

    public String getValue() {
        return this.name().toLowerCase();
    }
}

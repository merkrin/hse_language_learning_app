package com.example.languageapp.androidclient.response;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Optional;

public class FeedbackDto {
    @Min(value = 1, message = "Grade cannot be less than 1 or more than 5")
    @Max(value = 5, message = "Grade cannot be less than 1 or more than 5")
    private final int grade;

    private final Optional<String> review;

    @JsonCreator
    public FeedbackDto(
            @JsonProperty(required = true) int grade,
            @JsonProperty(defaultValue = "") Optional<String> review) {
        this.grade = grade;
        this.review = review;
    }

    public int getGrade() {
        return grade;
    }

    public Optional<String> getReview() {
        return review;
    }
}
